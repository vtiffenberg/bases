SELECT R.`Nombre`, R.`Apellido` 
FROM `Gobierno_UBA`.`Representante` R, `Gobierno_UBA`.`ComposicionSesionAsambleaUniversitaria` CSA
  WHERE R.`DNI` = CSA.`DNIParticipante` AND CSA.`NroRonda` = 1
  GROUP BY CSA.`DNIParticipante`
  ORDER BY COUNT(CSA.`AñoAsamblea`)
  LIMIT 5
;

