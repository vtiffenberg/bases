INSERT INTO LlamadoAAsamblea (AñoAsamblea) VALUES (2014)

SELECT * FROM LlamadoAAsamblea

INSERT INTO SesionAU (AñoAsamblea, NroRonda) VALUES (2014, 1)

SELECT * FROM SesionAU
 
SELECT * FROM ComposicionSesionAsambleaUniversitaria

INSERT INTO MandatoCDEstudiante (IdFacultad, AñoCDEstudiante) VALUES (1, 2013)

SELECT * FROM MandatoCDEstudiante
SELECT * FROM MandatoCDGraduado


SELECT R.`Nombre`, R.`Apellido`
FROM `Gobierno_UBA`.`Representante` R, `Gobierno_UBA`.
`ComposicionSesionAsambleaUniversitaria` CSA
WHERE R.`DNI` = CSA.`DNIParticipante` AND CSA.`NroRonda` = 1
GROUP BY CSA.`DNIParticipante`
ORDER BY COUNT(CSA.`AñoAsamblea`)
LIMIT 5;  
  
  
SELECT COUNT(*)
FROM `Gobierno_UBA`.`SesionAU` S
WHERE S.`NroRonda` = 3;


SELECT COUNT(*) * 100.0 / (SELECT COUNT(*) FROM `Gobierno_UBA`.`Graduado`)
FROM `Gobierno_UBA`.`Graduado` Grad
WHERE Grad.`GraduadoDeLaUBA`;