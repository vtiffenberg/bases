SET foreign_key_checks = 0;
truncate table MandatoCSEstudiante;
truncate table MandatoCSGraduado;
truncate table MandatoCSProfesor;
truncate table CSEstudianteEjercidoPor;
truncate table CSGraduadoEjercidoPor;
truncate table CSProfesorEjercidoPor;
SET foreign_key_checks = 1;

INSERT INTO MandatoCSEstudiante VALUES (2014);
INSERT INTO MandatoCSGraduado VALUES (2014);
INSERT INTO MandatoCSProfesor VALUES (2014);

INSERT INTO CSEstudianteEjercidoPor SELECT e.DNI, 2014 FROM ClaustroEstudiantes e
	WHERE e.DNI NOT IN (SELECT cd.DNI FROM EstudianteElectoCD cd WHERE AñoCDEstudiante = 2014)
	AND e.AñoInicio < 2014
	ORDER BY RAND()
	LIMIT 5
;
	
INSERT INTO CSGraduadoEjercidoPor SELECT g.DNI, 2014 FROM ClaustroGraduados g
	WHERE g.DNI NOT IN (SELECT cd.DNI FROM GraduadoElectoCD cd WHERE AñoCDGraduado = 2014)
	AND g.AñoInicio < 2014
	ORDER BY RAND()
	LIMIT 5
;

INSERT INTO CSProfesorEjercidoPor SELECT p.DNI, 2014 FROM ClaustroProfesores p
	WHERE p.DNI NOT IN (SELECT cd.DNI FROM ProfesorElectoCD cd WHERE AñoCDProfesor = 2014)
	AND p.AñoInicio < 2014
	ORDER BY RAND()
	LIMIT 5
;

