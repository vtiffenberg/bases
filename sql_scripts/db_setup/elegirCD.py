#encoding: utf-8
from sys import argv, stdin

filedesc = argv[1] if len(argv) == 2 else stdin

def agregarMandatoYElectos(fac, anio, claustro, claustros):
	print """INSERT INTO MandatoCD{0} (IdFacultad, AñoCd{0}) 
		SELECT IdFacultad, {1} FROM Facultad
		WHERE Nombre = '{2}'
		ON DUPLICATE KEY UPDATE MandatoCD{0}.IdFacultad = Facultad.IdFacultad
	;""".format(claustro, anio,fac)
	#elijo 4 estudiantes y los pongo en ese mandato
	print """INSERT INTO {0}ElectoCD (DNI, IdFacultad, AñoCD{0})
		SELECT DISTINCT c.DNI, c.IdFacultad, {1} FROM Claustro{2} c
		JOIN Facultad f ON f.IdFacultad = c.IdFacultad
		WHERE f.Nombre = '{3}'
		ORDER BY RAND()
		LIMIT 4
	;""".format(claustro, anio, claustros, fac)


print """
SET foreign_key_checks = 0;
truncate table MandatoCDEstudiante;
truncate table MandatoCDGraduado;
truncate table EstudianteElectoCD;
truncate table GraduadoElectoCD;
truncate table MandatoCDProfesor;
truncate table ProfesorElectoCD;
-- truncate table EstudianteCandidatoCD;
-- truncate table GraduadoCandidatoCD;
-- truncate table ProfesorCandidatoCD;
SET foreign_key_checks = 1;
"""

with filedesc as f:
	for linea in f:
		data = map(lambda t: t.rstrip(), linea.split(','))
		if len(data) > 0:
			for claustro in [('Estudiante', 'Estudiantes'), ('Graduado', 'Graduados')]:
				agregarMandatoYElectos(data[0], data[1], claustro[0], claustro[1])
			if len(data) == 3 and data[2] == 'P':
				agregarMandatoYElectos(data[0], data[1], 'Profesor', 'Profesores')
		

print """
--	INSERT INTO EstudianteCandidatoCD SELECT * FROM EstudianteElectoCD;
--	INSERT INTO GraduadoCandidatoCD SELECT * FROM GraduadoElectoCD;
--	INSERT INTO ProfesorCandidatoCD SELECT * FROM ProfesorElectoCD;
--	INSERT INTO EstudianteCandidatoCD SELECT e.DNI, e.IdFacultad, AñoCDEstudiante
--		FROM EstudianteElectoCD cd JOIN ClaustroEstudiantes e
--			ON cd.IdFacultad = e.IdFacultad AND e.AñoInicio < cd.AñoCDEstudiante
--		ORDER BY RAND()
--		LIMIT 3
--	ON DUPLICATE KEY UPDATE EstudianteCandidatoCD.DNI = cd.DNI;
 """




#print """
#	SELECT * FROM MandatoCDEstudiante;
#	SELECT * FROM EstudianteElectoCD ORDER BY AñoCDEstudiante, IdFacultad;
#	SELECT * FROM MandatoCDGraduado;
#	SELECT * FROM GraduadoElectoCD ORDER BY AñoCDGraduado, IdFacultad;
#	SELECT * FROM MandatoCDProfesor;
#	SELECT * FROM ProfesorElectoCD ORDER BY AñoCDProfesor, IdFacultad;
#"""
