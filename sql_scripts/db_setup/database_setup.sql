 CREATE SCHEMA `Gobierno_UBA`;

CREATE  TABLE `Gobierno_UBA`.`Representante` (
	`DNI` INT UNSIGNED NOT NULL ,
	`Nombre` VARCHAR(45) NOT NULL ,
	`Apellido` VARCHAR(45) NOT NULL ,
	PRIMARY KEY (`DNI`) );
  
CREATE  TABLE `Gobierno_UBA`.`Estudiante` (
	`DNI` INT UNSIGNED NOT NULL ,
	`LU` VARCHAR(45) NOT NULL ,
	PRIMARY KEY (`DNI`) ,
	INDEX `DNI_idx` (`DNI` ASC) ,
	CONSTRAINT `DNI_Alumno`
	FOREIGN KEY (`DNI` )
	REFERENCES `Gobierno_UBA`.`Representante` (`DNI` )
	ON DELETE CASCADE
	ON UPDATE CASCADE);
	
CREATE  TABLE `Gobierno_UBA`.`Graduado` (
	`DNI` INT UNSIGNED NOT NULL ,
	`GraduadoDeLaUBA` BIT NOT NULL ,
	`AñoDeGraduación` INT UNSIGNED NOT NULL ,
	PRIMARY KEY (`DNI`) ,
	INDEX `DNI_idx` (`DNI` ASC) ,
	CONSTRAINT `DNI_Graduado`
	FOREIGN KEY (`DNI` )
	REFERENCES `Gobierno_UBA`.`Representante` (`DNI` )
	ON DELETE CASCADE
	ON UPDATE CASCADE); 
	
CREATE  TABLE `Gobierno_UBA`.`Profesor` (
	`DNI` INT UNSIGNED NOT NULL ,
	`ProfesorDeLaUBA` BIT NOT NULL ,
	`Legajo` INT UNSIGNED NOT NULL ,
	PRIMARY KEY (`DNI`) ,
	INDEX `DNI_idx` (`DNI` ASC) ,
	CONSTRAINT `DNI_Profesor`
	FOREIGN KEY (`DNI` )
	REFERENCES `Gobierno_UBA`.`Representante` (`DNI` )
	ON DELETE CASCADE
	ON UPDATE CASCADE);
	
CREATE  TABLE `Gobierno_UBA`.`Facultad` (
	`IdFacultad` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
	`Nombre` VARCHAR(45) NOT NULL ,
	PRIMARY KEY (`IdFacultad`) );
	
CREATE  TABLE `Gobierno_UBA`.`Agrupacion` (
  `IdAgrupacion` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `NombreAgrupacion` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`IdAgrupacion`) );

CREATE  TABLE `Gobierno_UBA`.`ClaustroEstudiantes` (
  `DNI` INT UNSIGNED NOT NULL ,
  `IdAgrupacion` INT UNSIGNED NOT NULL ,
  `AñoInicio` INT UNSIGNED NOT NULL ,
  `AñoFin` INT UNSIGNED,
  `IdFacultad` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`DNI`, `IdAgrupacion`, `AñoInicio`) ,
  INDEX `DNI_idx` (`DNI` ASC) ,
  INDEX `IdAgrupacion_idx` (`IdAgrupacion` ASC) ,
  INDEX `IdFacultad_idx` (`IdFacultad` ASC) ,
  CONSTRAINT `DNI_Claustro_Estudiantes`
    FOREIGN KEY (`DNI` )
    REFERENCES `Gobierno_UBA`.`Estudiante` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `IdAgrupacion_Claustro_Estudiantes`
    FOREIGN KEY (`IdAgrupacion` )
    REFERENCES `Gobierno_UBA`.`Agrupacion` (`IdAgrupacion` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `IdFacultad_Claustro_Estudiantes`
    FOREIGN KEY (`IdFacultad` )
    REFERENCES `Gobierno_UBA`.`Facultad` (`IdFacultad` )
    ON DELETE CASCADE);


CREATE  TABLE `Gobierno_UBA`.`ClaustroGraduados` (
  `DNI` INT UNSIGNED NOT NULL ,
  `IdAgrupacion` INT UNSIGNED NOT NULL ,
  `AñoInicio` INT UNSIGNED NOT NULL ,
  `AñoFin` INT UNSIGNED,
  `IdFacultad` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`DNI`, `IdAgrupacion`, `AñoInicio`) ,
  INDEX `DNI_idx` (`DNI` ASC) ,
  INDEX `IdAgrupacion_idx` (`IdAgrupacion` ASC) ,
  INDEX `IdFacultad_idx` (`IdFacultad` ASC) ,
  CONSTRAINT `DNI_Claustro_Graduados`
    FOREIGN KEY (`DNI` )
    REFERENCES `Gobierno_UBA`.`Graduado` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `IdAgrupacion_Claustro_Graduados`
    FOREIGN KEY (`IdAgrupacion` )
    REFERENCES `Gobierno_UBA`.`Agrupacion` (`IdAgrupacion` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `IdFacultad_Claustro_Graduados`
    FOREIGN KEY (`IdFacultad` )
    REFERENCES `Gobierno_UBA`.`Facultad` (`IdFacultad` )
    ON DELETE CASCADE);


CREATE  TABLE `Gobierno_UBA`.`ClaustroProfesores` (
  `DNI` INT UNSIGNED NOT NULL ,
  `IdAgrupacion` INT UNSIGNED NOT NULL ,
  `AñoInicio` INT UNSIGNED NOT NULL ,
  `AñoFin` INT UNSIGNED,
  `IdFacultad` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`DNI`, `IdAgrupacion`, `AñoInicio`) ,
  INDEX `DNI_idx` (`DNI` ASC) ,
  INDEX `IdAgrupacion_idx` (`IdAgrupacion` ASC) ,
  INDEX `IdFacultad_idx` (`IdFacultad` ASC) ,
  CONSTRAINT `DNI_Claustro_Profesores`
    FOREIGN KEY (`DNI` )
    REFERENCES `Gobierno_UBA`.`Profesor` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `IdAgrupacion_Claustro_Profesores`
    FOREIGN KEY (`IdAgrupacion` )
    REFERENCES `Gobierno_UBA`.`Agrupacion` (`IdAgrupacion` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `IdFacultad_Claustro_Profesores`
    FOREIGN KEY (`IdFacultad` )
    REFERENCES `Gobierno_UBA`.`Facultad` (`IdFacultad` )
    ON DELETE CASCADE);


CREATE  TABLE `Gobierno_UBA`.`MandatoCDEstudiante` (
  `IdFacultad` INT UNSIGNED NOT NULL ,
  `AñoCDEstudiante` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`IdFacultad`, `AñoCDEstudiante`) ,
  INDEX `IdFacultad_idx` (`IdFacultad` ASC) ,
  CONSTRAINT `IdFacultad_CD_Estudiante`
    FOREIGN KEY (`IdFacultad` )
    REFERENCES `Gobierno_UBA`.`Facultad` (`IdFacultad` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE  TABLE `Gobierno_UBA`.`MandatoCDGraduado` (
  `IdFacultad` INT UNSIGNED NOT NULL ,
  `AñoCDGraduado` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`IdFacultad`, `AñoCDGraduado`) ,
  INDEX `IdFacultad_idx` (`IdFacultad` ASC) ,
  CONSTRAINT `IdFacultad_CD_Graduado`
    FOREIGN KEY (`IdFacultad` )
    REFERENCES `Gobierno_UBA`.`Facultad` (`IdFacultad` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE  TABLE `Gobierno_UBA`.`MandatoCDProfesor` (
  `IdFacultad` INT UNSIGNED NOT NULL ,
  `AñoCDProfesor` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`IdFacultad`, `AñoCDProfesor`) ,
  INDEX `IdFacultad_idx` (`IdFacultad` ASC) ,
  CONSTRAINT `IdFacultad_CD_Profesor`
    FOREIGN KEY (`IdFacultad` )
    REFERENCES `Gobierno_UBA`.`Facultad` (`IdFacultad` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);