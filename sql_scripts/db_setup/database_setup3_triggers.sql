DELIMITER |

CREATE TRIGGER tres AFTER INSERT
ON `SesionAU` FOR EACH ROW BEGIN
-- Arranco con comision directiva
INSERT INTO `ComposicionSesionAsambleaUniversitaria` 
	SELECT `AñoAsamblea` , `NroRonda` , `DNI` 
		FROM `SesionAU`, `EstudianteElectoCD`
		WHERE `SesionAU`.`AñoAsamblea` = `EstudianteElectoCD`.`AñoCDEstudiante`
			AND `SesionAU`.`AñoAsamblea` = NEW.`AñoAsamblea`
			AND `SesionAU`.`NroRonda` = NEW.`NroRonda`;

INSERT INTO `ComposicionSesionAsambleaUniversitaria`
	SELECT `AñoAsamblea` , `NroRonda` , `DNI` 
		FROM `SesionAU`, `GraduadoElectoCD`
		WHERE `SesionAU`.`AñoAsamblea` = `GraduadoElectoCD`.`AñoCDGraduado`
			AND `SesionAU`.`AñoAsamblea` = NEW.`AñoAsamblea`
			AND `SesionAU`.`NroRonda` = NEW.`NroRonda`;

INSERT INTO `ComposicionSesionAsambleaUniversitaria`
	SELECT `AñoAsamblea` , `NroRonda` , `DNI` 
		FROM `SesionAU`, `ProfesorElectoCD`
		WHERE `SesionAU`.`AñoAsamblea` = `ProfesorElectoCD`.`AñoCDProfesor`
			AND `SesionAU`.`AñoAsamblea` = NEW.`AñoAsamblea`
			AND `SesionAU`.`NroRonda` = NEW.`NroRonda`;

-- Ahora los Decanos
INSERT INTO `ComposicionSesionAsambleaUniversitaria`
	SELECT `AñoAsamblea`, `NroRonda`, `DNIDecano` 
		FROM `SesionAU`, `Mandato_Decano`
		WHERE `SesionAU`.`AñoAsamblea` = `Mandato_Decano`.`AñoMandatoDecano`
			AND `SesionAU`.`AñoAsamblea` = NEW.`AñoAsamblea`
			AND `SesionAU`.`NroRonda` = NEW.`NroRonda`;

-- Ahora el Consejo Superior
INSERT INTO `ComposicionSesionAsambleaUniversitaria`
	SELECT `AñoAsamblea` , `NroRonda` , `DNIEstudiante` 
		FROM `SesionAU`, `CSEstudianteEjercidoPor`
		WHERE `SesionAU`.`AñoAsamblea` = `CSEstudianteEjercidoPor`.`AñoMandatoCSE`
			AND `SesionAU`.`AñoAsamblea` = NEW.`AñoAsamblea`
			AND `SesionAU`.`NroRonda` = NEW.`NroRonda`;

INSERT INTO `ComposicionSesionAsambleaUniversitaria`
	SELECT `AñoAsamblea` , `NroRonda` , `DNIGraduado` 
		FROM `SesionAU`, `CSGraduadoEjercidoPor`
		WHERE `SesionAU`.`AñoAsamblea` = `CSGraduadoEjercidoPor`.`AñoMandatoCSG`
			AND `SesionAU`.`AñoAsamblea` = NEW.`AñoAsamblea`
			AND `SesionAU`.`NroRonda` = NEW.`NroRonda`;

INSERT INTO `ComposicionSesionAsambleaUniversitaria`
	SELECT `AñoAsamblea` , `NroRonda` , `DNIProfesor` 
		FROM `SesionAU`, `CSProfesorEjercidoPor`
		WHERE `SesionAU`.`AñoAsamblea` = `CSProfesorEjercidoPor`.`AñoMandatoCSP`
			AND `SesionAU`.`AñoAsamblea` = NEW.`AñoAsamblea`
			AND `SesionAU`.`NroRonda` = NEW.`NroRonda`;

-- el decano todavia no fue electo, y no participa, fin
END|


CREATE TRIGGER unoEstudiante AFTER INSERT
ON `MandatoCDEstudiante` FOR EACH ROW BEGIN
	IF (SELECT COUNT(*) FROM MandatoCDGraduado g 
		WHERE g.IdFacultad = NEW.IdFacultad and g.AñoCDGraduado = NEW.añoCDEstudiante)
	< 1 THEN		 
		INSERT INTO `MandatoCDGraduado` VALUES (NEW.IdFacultad, NEW.AñoCDEstudiante)
			ON DUPLICATE KEY UPDATE MandatoCDGraduado.IdFacultad = NEW.IdFacultad;
	END IF;
END|

CREATE TRIGGER unoGraduado AFTER INSERT
ON `MandatoCDGraduado` FOR EACH ROW BEGIN
	IF (SELECT COUNT(*) FROM MandatoCDEstudiante e 
		WHERE e.IdFacultad = NEW.IdFacultad and e.AñoCDEstudiante = NEW.AñoCDGraduado)
	< 1 THEN		 
		INSERT INTO `MandatoCDEstudiante` VALUES (NEW.IdFacultad, NEW.AñoCDGraduado)
			ON DUPLICATE KEY UPDATE MandatoCDEstudiante.IdFacultad = NEW.IdFacultad;
	END IF;
END|

CREATE TRIGGER unoProfesor AFTER INSERT
ON `MandatoCDProfesor` FOR EACH ROW BEGIN
INSERT INTO `MandatoCDGraduado` VALUES (NEW.IdFacultad, NEW.AñoCDProfesor)
	ON DUPLICATE KEY UPDATE MandatoCDGraduado.IdFacultad = NEW.IdFacultad;
INSERT INTO `MandatoCDEstudiante` VALUES (NEW.IdFacultad, NEW.AñoCDProfesor)
	ON DUPLICATE KEY UPDATE MandatoCDEstudiante.IdFacultad = NEW.IdFacultad;
END|


CREATE TRIGGER dos BEFORE INSERT
ON `SesionAU` FOR EACH ROW BEGIN
SET NEW.`NroRonda` = 1 + (SELECT IFNULL(MAX(`NroRonda`), 0) FROM `SesionAU` 
WHERE `SesionAU`.`AñoAsamblea` = NEW.`AñoAsamblea`);
END|

DELIMITER ;
