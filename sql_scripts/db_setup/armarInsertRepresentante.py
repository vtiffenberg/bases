#encoding: utf-8

from sys import argv, stdin
from random import choice, randrange


def insertClaustro(dni, claustro, anio):
	print "INSERT INTO Claustro{} (DNI, IdAgrupacion, AñoInicio, AñoFin, IdFacultad)".format(claustro)
	print "   SELECT {}, IdAgrupacion, {}, NULL, IdFacultad".format(dni, anio)
	print "   FROM Agrupacion, Facultad ORDER BY RAND() LIMIT 1;"

def insertEstudiante(dni):
	anioRdm = randrange(0,13)
	luRdm = "{}/{}".format(randrange(1,999), anioRdm)
	print "INSERT INTO Estudiante VALUES ({}, '{}')\n  ON DUPLICATE KEY UPDATE LU = {};".format(dni, luRdm, luRdm)
	insertClaustro(dni, 'Estudiantes', anioRdm+2000) 

deLaUBA = lambda:  1 if choice([True, False]) or choice([True, False]) or choice([True, False]) else 0 #1 de cada 8 dice que no

def insertGraduado(dni):
	anio = randrange(1990, 2013)
	print "INSERT INTO Graduado VALUES ({}, {}, {})\n  ON DUPLICATE KEY UPDATE AñoDeGraduación = {};".format(dni, deLaUBA(), anio, anio)
	insertClaustro(dni, 'Graduados', anio)

def insertProf(dni):
	legajo = randrange(111111, 999999)
	print "INSERT INTO Profesor VALUES ({}, {}, {})\n  ON DUPLICATE KEY UPDATE Legajo = {};".format(dni, deLaUBA(), legajo, legajo)
	insertClaustro(dni, 'Profesores', randrange(1990, 2010))



filedesc = argv[1] if len(argv) == 2 else stdin

with filedesc as rr:
	for l in rr:
		data = l.split(',')
		data = map(lambda l:l.rstrip(), data)
		print "INSERT INTO Representante VALUES ({}, '{}', '{}')\n  ON DUPLICATE KEY UPDATE Nombre = '{}';".format(data[2], data[1], data[0], data[0])
		choice([insertEstudiante, insertProf, insertGraduado])(data[2]) #ejecutar el insert en el claustro correspondiente
