/* FK no son tuplas desde acá */

CREATE  TABLE `Gobierno_UBA`.`VotosAgrupacionesEstudiantes` (
  `IdAgrupacion` INT UNSIGNED NOT NULL ,
  `IdFacultad` INT UNSIGNED NOT NULL ,
  `AñoCDEstudiante` INT UNSIGNED NOT NULL ,
  `VotosCDEstudiantes` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`IdAgrupacion`, `IdFacultad`, `AñoCDEstudiante`) ,
  INDEX `IdAgrupacion_idx` (`IdAgrupacion` ASC) ,
  INDEX `IdFacultad_idx` (`IdFacultad` ASC) ,
  INDEX `AñoCDEstudiante_idx` (`AñoCDEstudiante` ASC) ,
  CONSTRAINT `IdAgrupacion_Votos_Estudiantes`
    FOREIGN KEY (`IdAgrupacion` )
    REFERENCES `Gobierno_UBA`.`Agrupacion` (`IdAgrupacion` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `MandatoCDEstudiante_Votos_Estudiantes`
    FOREIGN KEY (`IdFacultad`, `AñoCDEstudiante` )
    REFERENCES `Gobierno_UBA`.`MandatoCDEstudiante` (`IdFacultad`, `AñoCDEstudiante` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE  TABLE `Gobierno_UBA`.`VotosAgrupacionesGraduados` (
  `IdAgrupacion` INT UNSIGNED NOT NULL ,
  `IdFacultad` INT UNSIGNED NOT NULL ,
  `AñoCDGraduado` INT UNSIGNED NOT NULL ,
  `VotosCDGraduados` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`IdAgrupacion`, `IdFacultad`, `AñoCDGraduado`) ,
  INDEX `IdAgrupacion_idx` (`IdAgrupacion` ASC) ,
  INDEX `IdFacultad_idx` (`IdFacultad` ASC) ,
  INDEX `AñoCDGraduado_idx` (`AñoCDGraduado` ASC) ,
  CONSTRAINT `IdAgrupacion_Votos_Graduados`
    FOREIGN KEY (`IdAgrupacion` )
    REFERENCES `Gobierno_UBA`.`Agrupacion` (`IdAgrupacion` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `MandatoCDGraduado_Votos_Graduados`
    FOREIGN KEY (`IdFacultad`,`AñoCDGraduado` )
    REFERENCES `Gobierno_UBA`.`MandatoCDGraduado` (`IdFacultad`,`AñoCDGraduado` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE  TABLE `Gobierno_UBA`.`VotosAgrupacionesProfesores` (
  `IdAgrupacion` INT UNSIGNED NOT NULL ,
  `IdFacultad` INT UNSIGNED NOT NULL ,
  `AñoCDProfesor` INT UNSIGNED NOT NULL ,
  `VotosCDProfesores` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`IdAgrupacion`, `IdFacultad`, `AñoCDProfesor`) ,
  INDEX `IdAgrupacion_idx` (`IdAgrupacion` ASC) ,
  INDEX `IdFacultad_idx` (`IdFacultad` ASC) ,
  INDEX `AñoCDProfesor_idx` (`AñoCDProfesor` ASC) ,
  CONSTRAINT `IdAgrupacion_Votos_Profesores`
    FOREIGN KEY (`IdAgrupacion` )
    REFERENCES `Gobierno_UBA`.`Agrupacion` (`IdAgrupacion` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `MandatoCDProfesor_Votos_Profesores`
    FOREIGN KEY (`IdFacultad`,`AñoCDProfesor` )
    REFERENCES `Gobierno_UBA`.`MandatoCDProfesor` (`IdFacultad`,`AñoCDProfesor` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE  TABLE `Gobierno_UBA`.`EstudianteElectoCD` (
  `DNI` INT UNSIGNED NOT NULL ,
  `IdFacultad` INT UNSIGNED NOT NULL ,
  `AñoCDEstudiante` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`DNI`, `IdFacultad`, `AñoCDEstudiante`) ,
  INDEX `DNI_idx` (`DNI` ASC) ,
  INDEX `AñoCDEstudiante_idx` (`IdFacultad` ASC, `AñoCDEstudiante` ASC) ,
  CONSTRAINT `DNI_Estudiante_Electo`
    FOREIGN KEY (`DNI` )
    REFERENCES `Gobierno_UBA`.`Estudiante` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ComisionEstudiante`
    FOREIGN KEY (`IdFacultad` , `AñoCDEstudiante` )
    REFERENCES `Gobierno_UBA`.`MandatoCDEstudiante` (`IdFacultad` , `AñoCDEstudiante` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);

/* hasta acá */

CREATE  TABLE `Gobierno_UBA`.`GraduadoElectoCD` (
  `DNI` INT UNSIGNED NOT NULL ,
  `IdFacultad` INT UNSIGNED NOT NULL ,
  `AñoCDGraduado` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`DNI`, `IdFacultad`, `AñoCDGraduado`) ,
  INDEX `DNI_idx` (`DNI` ASC) ,
  INDEX `AñoCDGraduado_idx` (`IdFacultad` ASC, `AñoCDGraduado` ASC) ,
  CONSTRAINT `DNI_Graduado_Electo`
    FOREIGN KEY (`DNI` )
    REFERENCES `Gobierno_UBA`.`Graduado` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ComisionGraduado`
    FOREIGN KEY (`IdFacultad` , `AñoCDGraduado` )
    REFERENCES `Gobierno_UBA`.`MandatoCDGraduado` (`IdFacultad` , `AñoCDGraduado` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE  TABLE `Gobierno_UBA`.`ProfesorElectoCD` (
  `DNI` INT UNSIGNED NOT NULL ,
  `IdFacultad` INT UNSIGNED NOT NULL ,
  `AñoCDProfesor` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`DNI`, `IdFacultad`, `AñoCDProfesor`) ,
  INDEX `DNI_idx` (`DNI` ASC) ,
  INDEX `AñoCDProfesor_idx` (`IdFacultad` ASC, `AñoCDProfesor` ASC) ,
  CONSTRAINT `DNI_Profesor_Electo`
    FOREIGN KEY (`DNI` )
    REFERENCES `Gobierno_UBA`.`Profesor` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ComisionProfesor`
    FOREIGN KEY (`IdFacultad` , `AñoCDProfesor` )
    REFERENCES `Gobierno_UBA`.`MandatoCDProfesor` (`IdFacultad` , `AñoCDProfesor` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE  TABLE `Gobierno_UBA`.`EstudianteCandidatoCD` (
  `DNI` INT UNSIGNED NOT NULL ,
  `IdFacultad` INT UNSIGNED NOT NULL ,
  `AñoCDEstudiante` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`DNI`, `IdFacultad`, `AñoCDEstudiante`) ,
  INDEX `DNI_idx` (`DNI` ASC) ,
  INDEX `AñoCDEstudiante_idx` (`IdFacultad` ASC, `AñoCDEstudiante` ASC) ,
  CONSTRAINT `DNI_Estudiante_Candidato`
    FOREIGN KEY (`DNI` )
    REFERENCES `Gobierno_UBA`.`Estudiante` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ComisionEstudianteCandidato`
    FOREIGN KEY (`IdFacultad` , `AñoCDEstudiante` )
    REFERENCES `Gobierno_UBA`.`MandatoCDEstudiante` (`IdFacultad` , `AñoCDEstudiante` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE  TABLE `Gobierno_UBA`.`GraduadoCandidatoCD` (
  `DNI` INT UNSIGNED NOT NULL ,
  `IdFacultad` INT UNSIGNED NOT NULL ,
  `AñoCDGraduado` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`DNI`, `IdFacultad`, `AñoCDGraduado`) ,
  INDEX `DNI_idx` (`DNI` ASC) ,
  INDEX `AñoCDGraduado_idx` (`IdFacultad` ASC, `AñoCDGraduado` ASC) ,
  CONSTRAINT `DNI_Graduado_Candidato`
    FOREIGN KEY (`DNI` )
    REFERENCES `Gobierno_UBA`.`Graduado` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ComisionGraduadoCandidato`
    FOREIGN KEY (`IdFacultad` , `AñoCDGraduado` )
    REFERENCES `Gobierno_UBA`.`MandatoCDGraduado` (`IdFacultad` , `AñoCDGraduado` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE  TABLE `Gobierno_UBA`.`ProfesorCandidatoCD` (
  `DNI` INT UNSIGNED NOT NULL ,
  `IdFacultad` INT UNSIGNED NOT NULL ,
  `AñoCDProfesor` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`DNI`, `IdFacultad`, `AñoCDProfesor`) ,
  INDEX `DNI_idx` (`DNI` ASC) ,
  INDEX `AñoCDProfesor_idx` (`IdFacultad` ASC, `AñoCDProfesor` ASC) ,
  CONSTRAINT `DNI_Profesor_Candidato`
    FOREIGN KEY (`DNI` )
    REFERENCES `Gobierno_UBA`.`Profesor` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ComisionProfesorCandidato`
    FOREIGN KEY (`IdFacultad` , `AñoCDProfesor` )
    REFERENCES `Gobierno_UBA`.`MandatoCDProfesor` (`IdFacultad` , `AñoCDProfesor` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE  TABLE `Gobierno_UBA`.`Mandato_Decano` (
  `IdFacultad` INT UNSIGNED NOT NULL ,
  `AñoMandatoDecano` INT UNSIGNED NOT NULL ,
  `DNIDecano` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`IdFacultad`, `AñoMandatoDecano`) ,
  INDEX `IdUniversidad_Mandato_idx` (`IdFacultad` ASC) ,
  INDEX `DNI_Decano_idx` (`DNIDecano` ASC) ,
  CONSTRAINT `IdFacultad_Mandato_Decano`
    FOREIGN KEY (`IdFacultad` )
    REFERENCES `Gobierno_UBA`.`Facultad` (`IdFacultad` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `DNI_Decano`
    FOREIGN KEY (`DNIDecano` )
    REFERENCES `Gobierno_UBA`.`Profesor` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE  TABLE `Gobierno_UBA`.`Voto_Decano` (
  `IdVotoDecano` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `DNIEmitio` INT UNSIGNED NOT NULL ,
  `DNIObtuvoVoto` INT UNSIGNED NULL ,
  `IdFacultad` INT UNSIGNED NOT NULL ,
  `AñoMandatoDecano` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`IdVotoDecano`) ,
  INDEX `Emitido_idx` (`DNIEmitio` ASC) ,
  INDEX `Recibido_idx` (`DNIObtuvoVoto` ASC) ,
  INDEX `MandatoDecano_idx` (`IdFacultad` ASC, `AñoMandatoDecano` ASC) ,
  CONSTRAINT `EmitidoDecano`
    FOREIGN KEY (`DNIEmitio` )
    REFERENCES `Gobierno_UBA`.`Representante` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `RecibidoDecano`
    FOREIGN KEY (`DNIObtuvoVoto` )
    REFERENCES `Gobierno_UBA`.`Profesor` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `MandatoDecano`
    FOREIGN KEY (`IdFacultad` , `AñoMandatoDecano` )
    REFERENCES `Gobierno_UBA`.`Mandato_Decano` (`IdFacultad` , `AñoMandatoDecano` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE  TABLE `Gobierno_UBA`.`MandatoCSEstudiante` (
  `AñoMandatoCSE` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`AñoMandatoCSE`) );


CREATE  TABLE `Gobierno_UBA`.`CSEstudianteEjercidoPor` (
  `DNIEstudiante` INT UNSIGNED NOT NULL ,
  `AñoMandatoCSE` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`DNIEstudiante`, `AñoMandatoCSE`) ,
  INDEX `MandatoCSE_idx` (`AñoMandatoCSE` ASC) ,
  INDEX `EstudianteEjerceCS_idx` (`DNIEstudiante` ASC) ,
  CONSTRAINT `MandatoCSE`
    FOREIGN KEY (`AñoMandatoCSE` )
    REFERENCES `Gobierno_UBA`.`MandatoCSEstudiante` (`AñoMandatoCSE` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `EstudianteEjerceCS`
    FOREIGN KEY (`DNIEstudiante` )
    REFERENCES `Gobierno_UBA`.`Estudiante` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE  TABLE `Gobierno_UBA`.`MandatoCSGraduado` (
  `AñoMandatoCSG` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`AñoMandatoCSG`) );

CREATE  TABLE `Gobierno_UBA`.`CSGraduadoEjercidoPor` (
  `DNIGraduado` INT UNSIGNED NOT NULL ,
  `AñoMandatoCSG` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`DNIGraduado`, `AñoMandatoCSG`) ,
  INDEX `MandatoCSE_idx` (`AñoMandatoCSG` ASC) ,
  INDEX `GraduadoEjerceCS_idx` (`DNIGraduado` ASC) ,
  CONSTRAINT `MandatoCSG`
    FOREIGN KEY (`AñoMandatoCSG` )
    REFERENCES `Gobierno_UBA`.`MandatoCSGraduado` (`AñoMandatoCSG` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `GraduadoEjerceCS`
    FOREIGN KEY (`DNIGraduado` )
    REFERENCES `Gobierno_UBA`.`Graduado` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE  TABLE `Gobierno_UBA`.`MandatoCSProfesor` (
  `AñoMandatoCSP` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`AñoMandatoCSP`) );

-- Esta tabla fallaba en INDEX `MandatoCSE_idx` (`AñoMandatoCSG` ASC). Dice que AñoMandatoCSG no existe
CREATE  TABLE `Gobierno_UBA`.`CSProfesorEjercidoPor` (
  `DNIProfesor` INT UNSIGNED NOT NULL ,
  `AñoMandatoCSP` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`DNIProfesor`, `AñoMandatoCSP`) ,
  INDEX `MandatoCSP_idx` (`AñoMandatoCSP` ASC) ,
  INDEX `ProfesorEjerceCS_idx` (`DNIProfesor` ASC) ,
  CONSTRAINT `MandatoCSP`
    FOREIGN KEY (`AñoMandatoCSP` )
    REFERENCES `Gobierno_UBA`.`MandatoCSProfesor` (`AñoMandatoCSP` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ProfesorEjerceCS`
    FOREIGN KEY (`DNIProfesor` )
    REFERENCES `Gobierno_UBA`.`Profesor` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE  TABLE `Gobierno_UBA`.`VotoDeCSEstudiante` (
  `IdVotoCSE` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `AñoMandatoCSE` INT UNSIGNED NOT NULL ,
  `DNIEmisor` INT UNSIGNED NOT NULL ,
  `DNIReceptor` INT UNSIGNED NULL ,
  PRIMARY KEY (`IdVotoCSE`) ,
  INDEX `CSEstudiante_idx` (`AñoMandatoCSE` ASC) ,
  INDEX `DNIEmisorEstudiante_idx` (`DNIEmisor` ASC) ,
  INDEX `DNIReceptorEstudiante_idx` (`DNIReceptor` ASC) ,
  CONSTRAINT `CSEstudiante`
    FOREIGN KEY (`AñoMandatoCSE` )
    REFERENCES `Gobierno_UBA`.`MandatoCSEstudiante` (`AñoMandatoCSE` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `DNIEmisorEstudiante`
    FOREIGN KEY (`DNIEmisor` )
    REFERENCES `Gobierno_UBA`.`Estudiante` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `DNIReceptorEstudiante`
    FOREIGN KEY (`DNIReceptor` )
    REFERENCES `Gobierno_UBA`.`Estudiante` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE  TABLE `Gobierno_UBA`.`VotoDeCSGraduado` (
  `IdVotoCSG` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `AñoMandatoCSG` INT UNSIGNED NOT NULL ,
  `DNIEmisor` INT UNSIGNED NOT NULL ,
  `DNIReceptor` INT UNSIGNED NULL ,
  PRIMARY KEY (`IdVotoCSG`) ,
  INDEX `CSGraduado_idx` (`AñoMandatoCSG` ASC) ,
  INDEX `DNIEmisorGraduado_idx` (`DNIEmisor` ASC) ,
  INDEX `DNIReceptorGraduado_idx` (`DNIReceptor` ASC) ,
  CONSTRAINT `CSGraduado`
    FOREIGN KEY (`AñoMandatoCSG` )
    REFERENCES `Gobierno_UBA`.`MandatoCSGraduado` (`AñoMandatoCSG` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `DNIEmisorGraduado`
    FOREIGN KEY (`DNIEmisor` )
    REFERENCES `Gobierno_UBA`.`Graduado` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `DNIReceptorGraduado`
    FOREIGN KEY (`DNIReceptor` )
    REFERENCES `Gobierno_UBA`.`Graduado` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE  TABLE `Gobierno_UBA`.`VotoDeCSProfesor` (
  `IdVotoCSP` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `AñoMandatoCSP` INT UNSIGNED NOT NULL ,
  `DNIEmisor` INT UNSIGNED NOT NULL ,
  `DNIReceptor` INT UNSIGNED NULL ,
  PRIMARY KEY (`IdVotoCSP`) ,
  INDEX `CSProfesor_idx` (`AñoMandatoCSP` ASC) ,
  INDEX `DNIEmisorProfesor_idx` (`DNIEmisor` ASC) ,
  INDEX `DNIReceptorProfesor_idx` (`DNIReceptor` ASC) ,
  CONSTRAINT `CSProfesor`
    FOREIGN KEY (`AñoMandatoCSP` )
    REFERENCES `Gobierno_UBA`.`MandatoCSProfesor` (`AñoMandatoCSP` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `DNIEmisorProfesor`
    FOREIGN KEY (`DNIEmisor` )
    REFERENCES `Gobierno_UBA`.`Profesor` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `DNIReceptorProfesor`
    FOREIGN KEY (`DNIReceptor` )
    REFERENCES `Gobierno_UBA`.`Profesor` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE  TABLE `Gobierno_UBA`.`LlamadoAAsamblea` (
  `AñoAsamblea` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`AñoAsamblea`) );

-- Generar nroRonda utomaticamente? se puede hacer con un motor viejo de base de datos (MyISAM) o.. con un trigger, esta segunda opcion es valida e igualmente se tiene que hacer para verificar q la ronda insertada es correcta, decidir que hacer.

CREATE  TABLE `Gobierno_UBA`.`SesionAU` (
  `AñoAsamblea` INT UNSIGNED NOT NULL ,
  `NroRonda` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`AñoAsamblea`, `NroRonda`) ,
  INDEX `AñoAsamblea_idx` (`AñoAsamblea` ASC) ,
  CONSTRAINT `AñoAsamblea`
    FOREIGN KEY (`AñoAsamblea` )
    REFERENCES `Gobierno_UBA`.`LlamadoAAsamblea` (`AñoAsamblea` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE  TABLE `Gobierno_UBA`.`ComposicionSesionAsambleaUniversitaria` (
  `AñoAsamblea` INT UNSIGNED NOT NULL ,
  `NroRonda` INT UNSIGNED NOT NULL ,
  `DNIParticipante` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`AñoAsamblea`, `NroRonda`, `DNIParticipante`) ,
  INDEX `RondaAsamblea_idx` (`AñoAsamblea` ASC, `NroRonda` ASC) ,
  INDEX `DNIParticipante_idx` (`DNIParticipante` ASC) ,
  CONSTRAINT `SesionAsamblea`
    FOREIGN KEY (`AñoAsamblea` , `NroRonda` )
    REFERENCES `Gobierno_UBA`.`SesionAU` (`AñoAsamblea` , `NroRonda` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `DNIParticipante`
    FOREIGN KEY (`DNIParticipante` )
    REFERENCES `Gobierno_UBA`.`Representante` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);


CREATE  TABLE `Gobierno_UBA`.`VotoDeRector` (
  `IdVotoRector` INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  `DNIReceptor` INT UNSIGNED NULL ,
  `AñoAsamblea` INT UNSIGNED NOT NULL ,
  `NroRonda` INT UNSIGNED NOT NULL ,
  `DNIRepresentante` INT UNSIGNED NOT NULL ,
  PRIMARY KEY (`IdVotoRector`) ,
  INDEX `DNIVotadoRector_idx` (`DNIReceptor` ASC) ,
  INDEX `ComposicionAsamblea_idx` (`AñoAsamblea` ASC, `NroRonda` ASC, `DNIRepresentante` ASC) ,
  CONSTRAINT `DNIVotadoRector`
    FOREIGN KEY (`DNIReceptor` )
    REFERENCES `Gobierno_UBA`.`Profesor` (`DNI` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `ComposicionAsamblea`
    FOREIGN KEY (`AñoAsamblea` , `NroRonda` , `DNIRepresentante` )
    REFERENCES `Gobierno_UBA`.`ComposicionSesionAsambleaUniversitaria` (`AñoAsamblea` , `NroRonda` , `DNIParticipante` )
    ON DELETE CASCADE
    ON UPDATE CASCADE);
