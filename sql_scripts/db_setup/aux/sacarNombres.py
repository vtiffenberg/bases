from sys import argv
import re

regex = re.compile("\d+\s+(?P<DNI>\d+)\s+(?P<apellido>(\w|\s)+)\,\s+(?P<nombre>(\w|\s)+)\s+(DNI|Pas).*$")

with open(argv[1]) as rr:
	for l in rr:
		matches = regex.match(l)
		if matches:
			d = matches.groupdict()
			print "{},{},{}".format(d['apellido'], d['nombre'], d['DNI'])