
# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt

def errorBars(cantValoresPorParametro, array_parametros, array_mediaError, array_dist_de_media_al_min, array_dist_de_media_al_max, arch_salida):
    #cantValoresPorParametro se usa sólo para poner en el gráfico de cuántos valores fue cada parámetro
    #array_parámetros = array de los valores de los parámetros
    # array_"algo"Error = resumen estadístico donde la posición depende de "array_parámetros"
    x = array_parametros
    y = array_mediaError

    plt.figure()
    plt.errorbar(x, y, yerr=[array_dist_de_media_al_min, array_dist_de_media_al_max])
    plt.title("Minimo error - media error - maximo error")
    plt.suptitle("Error bars con N="+str(cantValoresPorParametro)+" valores por parametro", fontsize=14, fontweight='bold')
    plt.xlabel('Parametro')
    plt.ylabel('Error medio')
    plt.grid(True)
    plt.savefig(arch_salida, bbox_inches='tight')

def errorBarsJuntos(cantValoresPorParametro, array_parametros, arrays_classic, arrays_DS, arrays_MDV, prefijo_arch_salida):
    #cantValoresPorParametro se usa sólo para poner en el gráfico de cuántos valores fue cada parámetro
    #array_parámetros = array de los valores de los parámetros
    # array_"algo"Error = resumen estadístico donde la posición depende de "array_parámetros"

   #Fijo las escalas dependiendo del archivo
    if(prefijo_arch_salida == 'graficos/casoNormal-todos'):
        ylim_igualdad = [0, 0.16]
        ylim_mayor = [0, 0.14]
    if(prefijo_arch_salida == 'graficos/casoUniforme-todos'):
        ylim_igualdad = [0, 0.05]
        ylim_mayor = [0, 0.07]
    plt.figure()
    plt.title("Minimo error - media error - maximo error")
    plt.suptitle("Error bars con N="+str(cantValoresPorParametro)+" valores por parametro", fontsize=14, fontweight='bold')

    #################GRÁFICO IGUALDAD############################333
    f, (axis1, axis2, axis3, axis4) = plt.subplots(4, sharex=True)
    axis1.grid(True)
    axis1.set_ylabel('Error medio')

    #Todos juntos
    axis1.errorbar(array_parametros, arrays_classic.array_media_error_igualdad, yerr=[arrays_classic.array_distMediaAlMin_error_igualdad, arrays_classic.array_distMediaAlMax_error_igualdad], label='Classic')
    axis1.errorbar(array_parametros, arrays_DS.array_media_error_igualdad, yerr=[arrays_DS.array_distMediaAlMin_error_igualdad, arrays_DS.array_distMediaAlMax_error_igualdad], label='DS')
    axis1.errorbar(array_parametros, arrays_MDV.array_media_error_igualdad, yerr=[arrays_MDV.array_distMediaAlMin_error_igualdad, arrays_MDV.array_distMediaAlMax_error_igualdad], label='MDV')
    axis1.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

   #Classic
    axis2.grid(True)
    axis2.set_xlim([0, 120])
    axis2.set_ylim(ylim_igualdad)
    axis2.set_ylabel('Error medio')
    axis2.errorbar(array_parametros, arrays_classic.array_media_error_igualdad, yerr=[arrays_classic.array_distMediaAlMin_error_igualdad, arrays_classic.array_distMediaAlMax_error_igualdad], label='Classic', fmt='b')
    axis2.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

    #DS SÓLO
    axis3.grid(True)
    axis3.set_xlim([0, 120])
    axis3.set_ylim(ylim_igualdad)
    axis3.set_ylabel('Error medio')
    axis3.errorbar(array_parametros, arrays_DS.array_media_error_igualdad, yerr=[arrays_DS.array_distMediaAlMin_error_igualdad, arrays_DS.array_distMediaAlMax_error_igualdad], label='DS', fmt='g')
    axis3.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

    #MDV SÓLO
    axis4.grid(True)
    axis4.set_xlim([0, 120])
    axis4.set_ylim(ylim_igualdad)
    axis4.set_xlabel('Parametro')
    axis4.set_ylabel('Error medio')
    axis4.errorbar(array_parametros, arrays_MDV.array_media_error_igualdad, yerr=[arrays_MDV.array_distMediaAlMin_error_igualdad, arrays_MDV.array_distMediaAlMax_error_igualdad], label='MDV', fmt='r')
    axis4.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

    plt.savefig(prefijo_arch_salida+"-igualdad.png", bbox_inches='tight')


    #################GRÁFICO MAYOR###########################
    plt.clf()

    f, (axis1, axis2, axis3, axis4) = plt.subplots(4, sharex=True)
    axis1.grid(True)
    axis1.set_ylabel('Error medio')

    #Todos juntos
    axis1.errorbar(array_parametros, arrays_classic.array_media_error_mayor, yerr=[arrays_classic.array_distMediaAlMin_error_mayor, arrays_classic.array_distMediaAlMax_error_mayor], label='Classic')
    axis1.errorbar(array_parametros, arrays_DS.array_media_error_mayor, yerr=[arrays_DS.array_distMediaAlMin_error_mayor, arrays_DS.array_distMediaAlMax_error_mayor], label='DS')
    axis1.errorbar(array_parametros, arrays_MDV.array_media_error_mayor, yerr=[arrays_MDV.array_distMediaAlMin_error_mayor, arrays_MDV.array_distMediaAlMax_error_mayor], label='MDV')
    axis1.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

   #Classic
    axis2.grid(True)
    axis2.set_xlim([0, 120])
    axis2.set_ylim(ylim_mayor)
    axis2.set_ylabel('Error medio')
    axis2.errorbar(array_parametros, arrays_classic.array_media_error_mayor, yerr=[arrays_classic.array_distMediaAlMin_error_mayor, arrays_classic.array_distMediaAlMax_error_mayor], label='Classic', fmt='b')
    axis2.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

    #DS SÓLO
    axis3.grid(True)
    axis3.set_xlim([0, 120])
    axis3.set_ylim(ylim_mayor)
    axis3.set_ylabel('Error medio')
    axis3.errorbar(array_parametros, arrays_DS.array_media_error_mayor, yerr=[arrays_DS.array_distMediaAlMin_error_mayor, arrays_DS.array_distMediaAlMax_error_mayor], label='DS', fmt='g')
    axis3.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

    #MDV SÓLO
    axis4.grid(True)
    axis4.set_xlim([0, 120])
    axis4.set_ylim(ylim_mayor)
    axis4.set_xlabel('Parametro')
    axis4.set_ylabel('Error medio')
    axis4.errorbar(array_parametros, arrays_MDV.array_media_error_mayor, yerr=[arrays_MDV.array_distMediaAlMin_error_mayor, arrays_MDV.array_distMediaAlMax_error_mayor], label='MDV', fmt='r')
    axis4.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

    plt.savefig(prefijo_arch_salida+"-mayor.png", bbox_inches='tight')


def histograma(valores,columna,arch_salida):
    plt.clf()
    n, bins, patches = plt.hist(valores, 50, normed=1, facecolor='green', alpha=0.75)

    plt.xlabel('Valor')
    plt.ylabel('Frecuencia Relativa')
    plt.title('Columna ' + columna)
    plt.grid(True)

    plt.savefig(arch_salida, bbox_inches='tight')
