# -*- coding: latin-1 -*-

# DATOS SOBRE ESTE SCRIPT
# 1) Está TODO hardcodeado (archivos, parámetros, valores...)
# 2) Dado un archivo, genera los gráficos para una lista de parámetros y una lista de valores
# 3) Asume que sólo hay 3 estimadores (4 si contaramos el Exacto)
# 4) Asume que no se necesitan parametrizar muchas cosas
# 5) Para cambiar funcionamiento, cambiar valores hardcodeados
# 6) Los valores hardcodeados están al final de todo

import estimators
from interfazbd import Database
import numpy
from scipy import stats

def calcularErrorPara(estimador, exacto, valor):
   #Calculo la estimación por estimador
   estimacion_igualdad = estimador.estimate_equal(valor)
   estimacion_mayor = estimador.estimate_greater(valor)
   #Calculo el valor real
   exacto_igualdad = exacto.estimate_equal(valor)
   exacto_mayor = exacto.estimate_greater(valor)
   #Calculo los errores:
   errorIgualdad = abs(estimacion_igualdad-exacto_igualdad)
   errorMayor = abs(estimacion_mayor-exacto_mayor)
   return errorIgualdad, errorMayor

def generarDatosPara(claseDelEstimador, archivoDB, tabla, col, exacto, parametro, valores):
   array_error_igualdad = []
   array_error_mayor = []
   estimador = claseDelEstimador(archivoDB, tabla, col, parametro)
   #Medir len(valores) muestras de error para el estimador, por igualdad y por mayor
   for valor in valores:
      errorIgual, errorMayor = calcularErrorPara(estimador, exacto, valor)
      array_error_igualdad.append(errorIgual)
      array_error_mayor.append(errorMayor)

   return array_error_igualdad, array_error_mayor

def generarCSVPara(archivoDB, tabla, col, parametros, valores):
  #Genera un CSV en stdout con los p-valores de los TTests: Columna, Parametro, Estimador1 vs Estimador2, mayor/igualdad, p-value
  exacto = estimators.ExactEstimator(archivoDB, tabla, col)
  prefijoArchivo_sin_extension = archivoDB.split('.')[0]
  prefijoArchivo_sin_path= prefijoArchivo_sin_extension.split('/')[1]
  prefijoArchivo = "graficos/" + prefijoArchivo_sin_path
  classicClass = estimators.ClassicHistogram
  dsClass = estimators.DistributionSteps
  mdvClass = estimators.MaxDiffVariation
  classic = {'igual': {}, 'mayor': {}}
  ds = {'igual': {}, 'mayor': {}}
  mdv = {'igual': {}, 'mayor': {}}
  from sys import stderr

  csvOut = lambda col, param, vs, busqueda, pVal: ",".join(map(str, [col, param, vs, busqueda, pVal]))
  pValue = lambda array1, array2: stats.ttest_rel(array1, array2)[1]

  for param in parametros:
    stderr.write("Midiendo para columna "+str(col)+" y parametro "+str(param)+"\n")
    classic['igual'][param], classic['mayor'][param] = generarDatosPara(classicClass, archivoDB, tabla, col, exacto, param, valores)
    ds['igual'][param], ds['mayor'][param]  = generarDatosPara(dsClass, archivoDB, tabla, col, exacto, param, valores)
    mdv['igual'][param], mdv['mayor'][param] = generarDatosPara(mdvClass, archivoDB, tabla, col, exacto, param, valores)
  
    stderr.write("Haciendo TTests\n")
    #"Comparando Classic vs DistributionSteps"
    print csvOut(col, param, "Classic vs DS", "Igualdad", pValue(classic['igual'][param],ds['igual'][param]))
    print csvOut(col, param, "Classic vs DS", "Mayor", pValue(classic['mayor'][param],ds['mayor'][param]))
  
    #"Comparando Classic vs MDV"
    print csvOut(col, param, "Classic vs MDV", "Igualdad", pValue(classic['igual'][param],mdv['igual'][param]))
    print csvOut(col, param, "Classic vs MDV", "Mayor", pValue(classic['mayor'][param],mdv['mayor'][param]))

    #"Comparando DistributionSteps vs MDV"
    print csvOut(col, param, "DS vs MDV", "Igualdad", pValue(ds['igual'][param],mdv['igual'][param]))
    print csvOut(col, param, "DS vs MDV", "Mayor", pValue(ds['mayor'][param],mdv['mayor'][param]))




#DESDE ACÁ SERÍA COMO EL "main":

columns = ['c0','c1','c2','c3','c4','c5','c6','c7','c8','c9']
# archivos = ['casoUniforme.sqlite3']
#parametrosClassic = [10, 20, 30, 50, 100]
#parametrosDS = [10, 20, 30, 50, 100]
#parametrosMDV = [10, 20, 30, 50, 100]
parametros = [10,50,100]

# Valores Uniforme
# valores = [-500, -300, -100, -50, 0, 50, 100, 300, 500]
# Valores Normal


tabla ='table1'

for col in columns:
    path_arch = "databases/db.sqlite3"
    db = Database(path_arch)
    max_value, min_value, total_tuples = db.select_rows("max( " + col + "), min( " + col + "), count( " + col + ")", tabla)[0]
    valores = numpy.random.random_integers(min_value, max_value, 50)  #50 enteros uniformemente distribuidos
    generarCSVPara(path_arch, tabla, col, parametros, valores)
