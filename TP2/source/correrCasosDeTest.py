
# -*- coding: utf-8 -*-

import estimators
from interfazbd import Database

path_archivos = 'databases/'

file_caso1 = path_archivos + 'caso1.sqlite3'
file_caso2 = path_archivos + 'caso2.sqlite3'
# file_caso3 = path_archivos + 'caso3.sqlite3'
#COMIENZO FUNCIONES AUXILIARES
def mostrar_pantalla(res_igual, res_mayor, igual_esperado, mayor_esperado):
  obtenido_por_igual = "Sel(=%d) : %3.2f" % (i,res_igual)
  obtenido_por_mayor = "Sel(>%d) : %3.2f" % (i,res_mayor)
  esperado_por_igual = "Sel(=%d) : %3.2f" % (i,igual_esperado)
  esperado_por_mayor = "Sel(>%d) : %3.2f" % (i,mayor_esperado)
  default = obtenido_por_igual  + " | " + obtenido_por_mayor + " ||| " + esperado_por_igual + " | " + esperado_por_mayor
  if (res_igual != igual_esperado or res_mayor != mayor_esperado):
      default = default + "-- ERROR"
  print default
#FIN FUNCIONES AUXILIARES

print "------CASO 1------"
print "-El valor '5' repetido 100 veces en la tabla."
print "-Dividido en 10 partes (parameter = 10)."
print "   Classic Histogram Obtenido   |||     Esperado"
aClassicEstimator = estimators.ClassicHistogram(file_caso1, 'table1', 'c1', parameter=10)
#aDistributionStepsEstimator = estimators.DistributionSteps('db.sqlite3', 'table1', 'c1', parameter=20)
for i in [3,4]:
  res_clasico_igual= aClassicEstimator.estimate_equal(i)
  res_clasico_mayor= aClassicEstimator.estimate_greater(i)
  igual_esperado = 0.00
  mayor_esperado = 1.00
  mostrar_pantalla(res_clasico_igual, res_clasico_mayor, igual_esperado, mayor_esperado)

for i in [5]:
  res_clasico_igual= aClassicEstimator.estimate_equal(i)
  res_clasico_mayor= aClassicEstimator.estimate_greater(i)
  igual_esperado = 1.00
  mayor_esperado = 0.00
  mostrar_pantalla(res_clasico_igual, res_clasico_mayor, igual_esperado, mayor_esperado)

for i in [6,7]:
  res_clasico_igual= aClassicEstimator.estimate_equal(i)
  res_clasico_mayor= aClassicEstimator.estimate_greater(i)
  igual_esperado = 0.00
  mayor_esperado = 0.00
  mostrar_pantalla(res_clasico_igual, res_clasico_mayor, igual_esperado, mayor_esperado)


print "   Distribution Steps Histogram Obtenido   |||     Esperado"
aDSEstimator = estimators.DistributionSteps(file_caso1, 'table1', 'c1', parameter=10)
#aDistributionStepsEstimator = estimators.DistributionSteps('db.sqlite3', 'table1', 'c1', parameter=20)
for i in [3,4]:
  res_DS_igual= aDSEstimator.estimate_equal(i)
  res_DS_mayor= aDSEstimator.estimate_greater(i)
  igual_esperado = 0.00
  mayor_esperado = 1.00
  mostrar_pantalla(res_DS_igual, res_DS_mayor, igual_esperado, mayor_esperado)

for i in [5]:
  res_DS_igual= aDSEstimator.estimate_equal(i)
  res_DS_mayor= aDSEstimator.estimate_greater(i)
  igual_esperado = 1.00
  mayor_esperado = 0.00
  mostrar_pantalla(res_DS_igual, res_DS_mayor, igual_esperado, mayor_esperado)

for i in [6,7]:
  res_DS_igual= aDSEstimator.estimate_equal(i)
  res_DS_mayor= aDSEstimator.estimate_greater(i)
  igual_esperado = 0.00
  mayor_esperado = 0.00
  mostrar_pantalla(res_DS_igual, res_DS_mayor, igual_esperado, mayor_esperado)


print "   MaxDiffVariation Histogram Obtenido   |||     Esperado"
aMDFEstimator = estimators.MaxDiffVariation(file_caso1, 'table1', 'c1', parameter=10, second_parameter=0.1)
#aDistributionStepsEstimator = estimators.DistributionSteps('db.sqlite3', 'table1', 'c1', parameter=20)
for i in [3,4]:
  res_MDF_igual= aMDFEstimator.estimate_equal(i)
  res_MDF_mayor= aMDFEstimator.estimate_greater(i)
  igual_esperado = 0.00
  mayor_esperado = 1.00
  mostrar_pantalla(res_MDF_igual, res_MDF_mayor, igual_esperado, mayor_esperado)

for i in [5]:
  res_MDF_igual= aMDFEstimator.estimate_equal(i)
  res_MDF_mayor= aMDFEstimator.estimate_greater(i)
  igual_esperado = 1.00
  mayor_esperado = 0.00
  mostrar_pantalla(res_MDF_igual, res_MDF_mayor, igual_esperado, mayor_esperado)

for i in [6,7]:
  res_MDF_igual= aMDFEstimator.estimate_equal(i)
  res_MDF_mayor= aMDFEstimator.estimate_greater(i)
  igual_esperado = 0.00
  mayor_esperado = 0.00
  mostrar_pantalla(res_MDF_igual, res_MDF_mayor, igual_esperado, mayor_esperado)

print "------CASO 2------"
print "-El valor '5' repetido 100 veces en la tabla."
print "-Dividido en 10 partes (parameter = 10)."
print "   Classic Histogram Obtenido   |||     Esperado"
aClassicEstimator = estimators.ClassicHistogram(file_caso2, 'table1', 'c1', parameter=10)
#aDistributionStepsEstimator = estimators.DistributionSteps('db.sqlite3', 'table1', 'c1', parameter=20)
for i in [-1,0]:
  res_clasico_igual= aClassicEstimator.estimate_equal(i)
  res_clasico_mayor= aClassicEstimator.estimate_greater(i)
  igual_esperado = 0.00
  mayor_esperado = 1.00
  mostrar_pantalla(res_clasico_igual, res_clasico_mayor, igual_esperado, mayor_esperado)

for i in [1]:
  res_clasico_igual= aClassicEstimator.estimate_equal(i)
  res_clasico_mayor= aClassicEstimator.estimate_greater(i)
  igual_esperado = 0.06
  mayor_esperado = 0.94
  mostrar_pantalla(res_clasico_igual, res_clasico_mayor, igual_esperado, mayor_esperado)

for i in [2]:
  res_clasico_igual= aClassicEstimator.estimate_equal(i)
  res_clasico_mayor= aClassicEstimator.estimate_greater(i)
  igual_esperado = 0.06
  mayor_esperado = 0.88
  mostrar_pantalla(res_clasico_igual, res_clasico_mayor, igual_esperado, mayor_esperado)

for i in [3]:
  res_clasico_igual= aClassicEstimator.estimate_equal(i)
  res_clasico_mayor= aClassicEstimator.estimate_greater(i)
  igual_esperado = 0.13
  mayor_esperado = 0.75
  mostrar_pantalla(res_clasico_igual, res_clasico_mayor, igual_esperado, mayor_esperado)

for i in [4]:
  res_clasico_igual= aClassicEstimator.estimate_equal(i)
  res_clasico_mayor= aClassicEstimator.estimate_greater(i)
  igual_esperado = 0.25
  mayor_esperado = 0.50
  mostrar_pantalla(res_clasico_igual, res_clasico_mayor, igual_esperado, mayor_esperado)

for i in [5]:
  res_clasico_igual= aClassicEstimator.estimate_equal(i)
  res_clasico_mayor= aClassicEstimator.estimate_greater(i)
  igual_esperado = 0.50
  mayor_esperado = 0.00
  mostrar_pantalla(res_clasico_igual, res_clasico_mayor, igual_esperado, mayor_esperado)

for i in [6,7]:
  res_clasico_igual= aClassicEstimator.estimate_equal(i)
  res_clasico_mayor= aClassicEstimator.estimate_greater(i)
  igual_esperado = 0.00
  mayor_esperado = 0.00
  mostrar_pantalla(res_clasico_igual, res_clasico_mayor, igual_esperado, mayor_esperado)


print "   Distribution Steps Histogram Obtenido   |||     Esperado"
aDSEstimator = estimators.DistributionSteps(file_caso2, 'table1', 'c1', parameter=10)
#aDistributionStepsEstimator = estimators.DistributionSteps('db.sqlite3', 'table1', 'c1', parameter=20)
for i in [-1,0]:
  res_DS_igual= aDSEstimator.estimate_equal(i)
  res_DS_mayor= aDSEstimator.estimate_greater(i)
  igual_esperado = 0.00
  mayor_esperado = 1.00
  mostrar_pantalla(res_DS_igual, res_DS_mayor, igual_esperado, mayor_esperado)

for i in [1]:
  res_DS_igual= aDSEstimator.estimate_equal(i)
  res_DS_mayor= aDSEstimator.estimate_greater(i)
  igual_esperado = 0.05
  mayor_esperado = 0.95
  mostrar_pantalla(res_DS_igual, res_DS_mayor, igual_esperado, mayor_esperado)

for i in [2]:
  res_DS_igual= aDSEstimator.estimate_equal(i)
  res_DS_mayor= aDSEstimator.estimate_greater(i)
  igual_esperado = 0.10
  mayor_esperado = 0.85
  mostrar_pantalla(res_DS_igual, res_DS_mayor, igual_esperado, mayor_esperado)

for i in [3]:
  res_DS_igual= aDSEstimator.estimate_equal(i)
  res_DS_mayor= aDSEstimator.estimate_greater(i)
  igual_esperado = 0.10
  mayor_esperado = 0.75
  mostrar_pantalla(res_DS_igual, res_DS_mayor, igual_esperado, mayor_esperado)

for i in [4]:
  res_DS_igual= aDSEstimator.estimate_equal(i)
  res_DS_mayor= aDSEstimator.estimate_greater(i)
  igual_esperado = 0.20
  mayor_esperado = 0.55
  mostrar_pantalla(res_DS_igual, res_DS_mayor, igual_esperado, mayor_esperado)

for i in [5]:
  res_DS_igual= aDSEstimator.estimate_equal(i)
  res_DS_mayor= aDSEstimator.estimate_greater(i)
  igual_esperado = 0.55
  mayor_esperado = 0.00
  mostrar_pantalla(res_DS_igual, res_DS_mayor, igual_esperado, mayor_esperado)

for i in [6,7]:
  res_DS_igual= aDSEstimator.estimate_equal(i)
  res_DS_mayor= aDSEstimator.estimate_greater(i)
  igual_esperado = 0.00
  mayor_esperado = 0.00
  mostrar_pantalla(res_DS_igual, res_DS_mayor, igual_esperado, mayor_esperado)

print "   MaxDiffVariation Histogram Obtenido   |||     Esperado"
aMDFEstimator = estimators.MaxDiffVariation(file_caso2, 'table1', 'c1', parameter=10, second_parameter=0.1)
for i in [-1,0]:
  res_DS_igual= aMDFEstimator.estimate_equal(i)
  res_DS_mayor= aMDFEstimator.estimate_greater(i)
  igual_esperado = 0.00
  mayor_esperado = 1.00
  mostrar_pantalla(res_DS_igual, res_DS_mayor, igual_esperado, mayor_esperado)

for i in [1]:
  res_DS_igual= aMDFEstimator.estimate_equal(i)
  res_DS_mayor= aMDFEstimator.estimate_greater(i)
  igual_esperado = 0.05
  mayor_esperado = 0.95
  mostrar_pantalla(res_DS_igual, res_DS_mayor, igual_esperado, mayor_esperado)

for i in [2]:
  res_DS_igual= aMDFEstimator.estimate_equal(i)
  res_DS_mayor= aMDFEstimator.estimate_greater(i)
  igual_esperado = 0.10
  mayor_esperado = 0.85
  mostrar_pantalla(res_DS_igual, res_DS_mayor, igual_esperado, mayor_esperado)

for i in [3]:
  res_DS_igual= aMDFEstimator.estimate_equal(i)
  res_DS_mayor= aMDFEstimator.estimate_greater(i)
  igual_esperado = 0.10
  mayor_esperado = 0.75
  mostrar_pantalla(res_DS_igual, res_DS_mayor, igual_esperado, mayor_esperado)

for i in [4]:
  res_DS_igual= aMDFEstimator.estimate_equal(i)
  res_DS_mayor= aMDFEstimator.estimate_greater(i)
  igual_esperado = 0.20
  mayor_esperado = 0.55
  mostrar_pantalla(res_DS_igual, res_DS_mayor, igual_esperado, mayor_esperado)

for i in [5]:
  res_DS_igual= aMDFEstimator.estimate_equal(i)
  res_DS_mayor= aMDFEstimator.estimate_greater(i)
  igual_esperado = 0.55
  mayor_esperado = 0.00
  mostrar_pantalla(res_DS_igual, res_DS_mayor, igual_esperado, mayor_esperado)

for i in [6,7]:
  res_DS_igual= aMDFEstimator.estimate_equal(i)
  res_DS_mayor= aMDFEstimator.estimate_greater(i)
  igual_esperado = 0.00
  mayor_esperado = 0.00
  mostrar_pantalla(res_DS_igual, res_DS_mayor, igual_esperado, mayor_esperado)
