# coding=utf-8
import estimators
from interfazbd import Database
import numpy
import graficadores as graf
from interfazbd import Database


def generarImagenDistribuciones():
  print "Generando distribucion de tabla de la cátedra"
  prefijoArchivo = "distribuciones/distribucionColumna"
  db = Database('databases/db.sqlite3')
  columnas = ['c0','c1','c2','c3','c4','c5','c6','c7','c8','c9']
  for c in columnas:
    values = flattener(db.select_rows(c, "table1"))
    graf.histograma(values, c, prefijoArchivo + c + ".png")

def flattener(lst):
    return [item for sublist in lst for item in sublist]

generarImagenDistribuciones()
