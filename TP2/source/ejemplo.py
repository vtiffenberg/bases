# -*- coding: utf-8 -*-

import estimators
from interfazbd import Database

# Creo una instancia de la clase que representa al metodo
# 'Histograma Clasico'
aEstimator = estimators.ClassicHistogram('db.sqlite3', 'table1', 'c1', parameter=20)

# Pruebo distintas instancias de estimacion
print "Classic Histogram"
print "  Sel(=%d) : %3.2f" % (50, aEstimator.estimate_equal(50))
print "  Sel(>%d) : %3.2f" % (70, aEstimator.estimate_greater(70))

# Creo una instancia de la clase que representa al metodo
# 'Steps distribuidos
aEstimator = estimators.DistributionSteps('db.sqlite3', 'table1', 'c1', parameter=20)

# Pruebo distintas instancias de estimacion
print "Distribution Steps Histogram"
print "  Sel(=%d) : %3.2f" % (50, aEstimator.estimate_equal(50))
print "  Sel(>%d) : %3.2f" % (70, aEstimator.estimate_greater(70))

# Creo una instancia de la clase que representa al metodo
# 'Steps distribuidos
aEstimator = estimators.MaxDiffVariation('db.sqlite3', 'table1', 'c1', parameter=20, second_parameter=0.1)

# Pruebo distintas instancias de estimacion
print "MaxDiff Variation Histogram"
print "  Sel(=%d) : %3.2f" % (50, aEstimator.estimate_equal(50))
print "  Sel(>%d) : %3.2f" % (70, aEstimator.estimate_greater(70))


# Selectividad real
db = Database('db.sqlite3')
table = 'table1'
total_rows = db.select_rows("count(c1)", table)[0][0]

print "Selectividad real para el conjunto"
sel_50 = float(db.select_rows('count(c1)', table, ' WHERE c1 = 50')[0][0]) / float(total_rows)
sel_70 = float(db.select_rows('count(c1)', table, ' WHERE c1 > 70')[0][0]) / float(total_rows)
print "  Sel(=%d) : %3.2f" % (50, sel_50)
print "  Sel(>%d) : %3.2f" % (70, sel_70)
