#!/usr/bin/python
# -*- coding: latin-1 -*-

import argparse
import estimators
import os
from interfazbd import Database

#HELP:hacer:
#           python generarEstadisticas.py -h
modo_de_uso = """ Este script de python recibe dos argumentos y en base a ellos obtiene datos
estadisticos.
Formato de salida:
*) Cada fila representa a los datos obtenidos de un valor
*) Cada fila esta compuesta por 4 columnas:
.... Porcentaje (%) de tuplas para Clasico, DS, 3ro y DatosReales
.... Porcentaje (%) de tuplas para Distribution StepsS, 3ro y DatosReales
.... Porcentaje (%) de tuplas 3er estimador (provisto por nosotros)
.... Porcentaje (%) real en la tabla"""

ejemplo_de_uso= "python generarEstadisticas.py archivitoSalida 10 tablaParaCasosDeTest/caso1.db tablaPrueba1 c1 3 4 5 6 7"

parser = argparse.ArgumentParser(description='Este script de python recibe dos argumentos y en base a ellos obtiene datos estadisticos')
parser.add_argument('archSalida', metavar='archSalida', type=str, nargs=1,
                           help='Archivo donde se guardarán los datos.')
parser.add_argument('parametro', metavar='param', type=int, nargs=1,
                           help='Cantidad de veces a dividir los histogramas')
parser.add_argument('archivoDB', metavar='db', type=str, nargs=1,
                           help='Un path relativo al archivo desde el PATH actual ')
parser.add_argument('nombreTabla', metavar='tabla', type=str, nargs=1,
                           help='nombre de la tabla en la Base De Datos')
parser.add_argument('nombreColumna', metavar='col', type=str, nargs=1,
                           help='nombre de la columna en la tabla')
parser.add_argument('integers', metavar='N', type=int, nargs='+',
                           help='una lista de valores (ints) a procesar')

args = parser.parse_args()
archSalida = args.archSalida[0]
parametro = args.parametro[0]
archivoDB = args.archivoDB[0]
tabla = args.nombreTabla[0]
col = args.nombreColumna[0]
valores = args.integers

# DESDE ACA BORRAR
#print archSalida
#print parametro
#print archivoDB
#print tabla
#print col
#print valores
# HASTA ACA BORRAR

#Variables globales para usar en el Real
db = Database(archivoDB)
total_tuplas = db.select_rows("count(%s" % (col)+")", tabla)[0][0]

#FUNCIÓN AUXILIAR:
def mayor_real(valor):
        #ASUME QUE BASE DE DATOS ESTÁ CREADA EN db
        str_select = "count(%s" % (col)+")"
        str_guarda = " WHERE %s > %d" % (col, valor)
        frecuencia =  float(db.select_rows(str_select, tabla, str_guarda)[0][0])
        return frecuencia/total_tuplas
def igual_real(valor):
        #ASUME QUE BASE DE DATOS ESTÁ CREADA EN db
        str_select = "count(%s" % (col)+")"
        str_guarda = " WHERE %s = %d" % (col, valor)
        frecuencia =  float(db.select_rows(str_select, tabla, str_guarda)[0][0])
        return frecuencia/total_tuplas

#FIN FUNCIÓN AUXILIAR



print "Empezando a correr el script"
print "Creando los estimadores para la tabla en el path relativo: " + "tabla"
print "Histograma clásico ...."
classic = estimators.ClassicHistogram(archivoDB, tabla, col, parametro)
print "Histograma Distribution Steps ...."
ds = estimators.DistributionSteps(archivoDB, tabla, col, parametro)
print "Histograma 3 (nuestro) ...."
mdv = estimators.MaxDiffVariation(archivoDB, tabla, col, parametro, 0.1)
print "Terminados de crear todos los histogramas"

print "Empezando a correr para los valores ..."
lista_de_str_final = ["ClassicIgualdad ClassicMayor DistributionStepsIgualdad DistributionStepsMayor MaxDiffVariationIgualdad MaxDiffVariationMayor RealIgualdad RealMayor"]
for val in valores:
    print "Valor: " + str(val) + "..."
    nueva_linea = []
    print "....Classic...."
    res_classic_igualdad = classic.estimate_equal(val)
    res_classic_mayor = classic.estimate_greater(val)
    print "....DistributionSteps...."
    res_ds_igualdad = ds.estimate_equal(val)
    res_ds_mayor = ds.estimate_greater(val)
    print "....MaxDiffVariation...."
    res_mdv_igualdad = mdv.estimate_equal(val)
    res_mdv_mayor = mdv.estimate_greater(val)
    print "....Real...."
    res_real_igualdad = igual_real(val)
    res_real_mayor = mayor_real(val)
    #AL PONER AL MDV PONER LA LIŃEA SIGUIENTE NO LA SIGUIENTE A LA SIGUIENTE
    stringuito = str(res_classic_igualdad) + " " +  str(res_classic_mayor) + " " +  str(res_ds_igualdad) + " " +  str(res_ds_mayor) + " " +  str(res_mdv_igualdad) + " " +  str(res_mdv_mayor)  + " " + str(res_real_igualdad) + " " +  str(res_real_mayor)
    # stringuito = str(res_classic_igualdad) + " " +  str(res_classic_mayor) + " " +  str(res_ds_igualdad) + " " +  str(res_ds_mayor) + " " + str(res_real_igualdad) + " " +  str(res_real_mayor)
    lista_de_str_final.append(stringuito)

output = open(archSalida, 'w')
for fila in lista_de_str_final:
    output.write(fila+"\n")
