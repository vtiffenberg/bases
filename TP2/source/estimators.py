# -*- coding: utf-8 -*-

import sqlite3
import numpy as np
import pylab
import random
from interfazbd import Database

class Estimator(object):
    """Clase base de los estimadores."""

    def __init__(self, db, table, column, parameter=10, second_parameter=0.1):
        self.db = Database(db)
        self.table = table
        self.column = column
        self.parameter = parameter
        self.second_parameter = second_parameter

        # Assert table and column exist
        assert self.table in self.db.list_tables(), "Nonexistant table"
        assert self.column in self.db.list_columns_for(self.table), "Nonexistant column"
        assert self.db.column_type(self.table, self.column) == 'int', "Column type should be int"

        # Construye las estructuras necesita el estimador.
        self.build_struct()

    def bin_for(self, value):
        """  Busca entre tus steps (o bin, o bucket) el step que contenga ese valor.
        Es decir, dado un step obtenido con esta función puedo obtener el valor representante de
        ese step y, además, la cantidad de tuplas (filas en la columna) que entran en ese step"""
        max_value = self.keys[len(self.keys) - 1]
        min_value = self.keys[0]
        is_out_of_range = value >= max_value or value < min_value
        if is_out_of_range:
            if value < min_value:
                #Si es menor al mínimo, ERRRRRRRROR
                assert False, "Mal 'uso del bin_for'. Value (" + str(value) + ") es menor al mínimo (" + str(min_value) + "). Se debe asegurar que es mayor antes de usar esta función"
            if value > max_value:
                #Si es mayor al máximo, ERRRRRRRROR
                assert False, "Mal 'uso del bin_for'. Value (" + str(value) + ") es mayor al máximo (" + str(max_value) + "). Se debe asegurar que es menor antes de usar esta función"

        assert len(self.keys) > 0, 'La longitud de self.keys es vacía. Hay algo mal'

        if (value == max_value):
            return len(self.keys) - 1

        if len(self.keys) == 1:
            return 0

        low = 0
        high = len(self.keys) - 1
        middle = len(self.keys) / 2
        while (low < high):
            if self.keys[low] <= value < self.keys[low+1]:
                return low
            elif value >= self.keys[middle]:
                low = middle
            else:
                high = middle
            middle = (low + high) / 2
        #por aca sale si low = high = middle
        print "Esto esta mal"
        print low
        assert false, 'no deberia estar aca'
        return low

#METODOS ABSTRACTOS:
    def build_struct(self):
        raise NotImplementedError()

    def estimate_equal(self, value):
        raise NotImplementedError()

    def estimate_greater(self, value):
        raise NotImplementedError()


class ClassicHistogram(Estimator):
    """Histograma clasico"""
    #keys: es una lista de 'valores representantes' ordenados de menor a mayor. keys[2] es el
    #     valor que representa a los valores en el rango keys[2] a keys[3]-1.
    #bins: dado el 'valor representante' te dice la cantidad de filas (o tuplas) de la tabla que
    #     son 'repreentados por ese valor'.

    def build_struct(self):
        # Find min and max
        max_value, min_value, self.total_tuples = self.db.select_rows("max( " + self.column + "), min( " + self.column + "), count( " + self.column + ")", self.table)[0]

        # Build bins array as: {bin_start: amount_of_tuples}
        a_float = (max_value - min_value + 1.0) / self.parameter
        #assert a_float >= 1, 'Parametro (cantidad de veces a dividir) demasiado grande. Debe ser menor o igual a: '+ str( (max_value - min_value)+1)
        self.step_tuples = int(np.ceil(a_float))

        self.keys = []
        self.bins = {}
        for i in range(0, self.parameter):
            bin = min_value + i*self.step_tuples
            next_bin = bin + self.step_tuples
            where_clause = " WHERE %s >= %d AND %s < %d" % (self.column,bin,self.column,next_bin)
            self.bins[bin] = self.db.select_rows("count( " + self.column + ")", self.table, where_clause)[0][0]
            self.keys.append(bin)
        #print "Struct de Classic"
        #print self.keys
        #print self.bins

    def estimate_equal(self, value):
        """Preguntarle qué porcentaje de la tabla tiene este valor"""
        max_value = self.keys[len(self.keys) - 1]
        min_value = self.keys[0]
        is_out_of_range = value >= max_value or value < min_value
        if is_out_of_range:
            return 0.0
        bin = self.bin_for(value)
        valor_representante_del_bin = self.keys[bin]
        #si cada step tiene un solo valor, se que todo el bucket tiene un unico valor y no acoto con el 50%
        if self.step_tuples == 1:
            bound = 1.0
        else:
            bound = 0.5
        return self.bins[valor_representante_del_bin]*bound/self.total_tuples

    def estimate_greater(self, value):
        """Preguntarle qué pocentaje de la tabla tiene más que este valor (MAYORES ESTRICTOS) """
        max_value = self.keys[len(self.keys) - 1]
        min_value = self.keys[0]
        is_out_of_range = value >= max_value or value < min_value
        if is_out_of_range:
            if value >= max_value:
                return 0.0
            if value < min_value:
                return 1.0
        bin = self.bin_for(value)
        valor_representante = self.keys[bin]
        if ((value == (valor_representante + self.step_tuples -1)) or (self.step_tuples == 1)):
        #Si me piden el último valor válido de un bucket, no cuentan
        #los valores del bucket (último valor válido = máximo -1)
        #Tampoco si hay un unico valor en el bucket, lo descarto
            res = 0.0
        else:
            res = self.bins[self.keys[bin]]*0.5
        for index in self.keys[(bin+1)::]:
            res += self.bins[index]
        return res/self.total_tuples



class DistributionSteps(Estimator):
    """ Histograma según Piatetsky-Shapiro """
    #keys: es una lista de 'valores representantes' ordenados de menor a mayor. keys[2] es el
    #     valor que representa a los valores en el rango keys[2] a keys[3]-1.
    #bins: dado el 'valor representante' te dice la cantidad de filas (o tuplas) de la tabla que
    #     son 'repreentados por ese valor'.


    def build_struct(self):
        # Find min and max
        max_value, min_value, self.total_tuples = self.db.select_rows("max( " + self.column + "), min( " + self.column + "), count( " + self.column + ")", self.table)[0]

        # Build bins array as: {bin_start: amount_of_tuples}
        self.step_tuples = int(np.ceil(self.total_tuples/self.parameter))
        self.keys = []

        for i in range(0, self.parameter):
            where_clause = " ORDER BY %s ASC LIMIT 1 OFFSET %d" % (self.column,i*self.step_tuples)
            bin = self.db.select_rows(self.column, self.table, where_clause)[0][0]
            self.keys.append(bin)

        #self.bins[max_value]=0
        self.keys.append(max_value)
        assert len(self.keys) == self.parameter + 1, 'La longitud de self.keys debe tener un valro mas que la cantidad de buckets'

        #el ultimo bin puede tener menos valores
        # if (self.parameter>1):
        #     where_clause = " ORDER BY %s ASC OFFSET %d" % (self.column, (self.parameter - 1) * self.step_tuples)
        #     this.last_bin_size = self.db.select_rows("count( " + self.column + ") ", self.table, where_clause)[0][0]
        #     assert this.last_bin_size <= self.step_tuples, 'no puede haber mas de step tuples en el ultimo bin'


        #print "Struct de DistributionSteps"
        #print self.keys

    def estimate_equal(self, value):
        max_value = self.keys[self.parameter]
        min_value = self.keys[0]
        is_out_of_range = value > max_value or value < min_value
        if is_out_of_range:
            return 0.0
        bin = self.bin_for(value)

        I = 0.0
        if (self.keys[bin] != value):
            return (1.0/3.0)/self.parameter
        if (bin < (len(self.keys) - 1)):
            I = 1.0/2.0
        for i in range(bin,0,-1):
            if (self.keys[i] == self.keys[i-1]):
                I += 1.0
            else:
                I += 1.0/2.0
                break
        return I/self.parameter

    def estimate_greater(self, value):
        max_value = self.keys[len(self.keys) - 1]
        min_value = self.keys[0]
        is_out_of_range = value >= max_value or value < min_value
        if is_out_of_range:
            if value >= max_value:
                return 0.0
            if value < min_value:
                return 1.0
        bin = self.bin_for(value)
        I = 0.0
        if (bin == (len(self.keys)-1)):
            return 0.0

        if (bin < (len(self.keys) - 1)):
            I = self.parameter - bin - 1

        if (self.keys[bin] != value):
            I += 1.0/3.0
        else:
            I += 1.0/2.0
        return I/self.parameter



class MaxDiffVariation(Estimator):
    """ Poosala, Haas, Ioannidis, Shekita: Improved Histograms for Selectivity Estimation of Range Predicates
    La variación consiste en tomar un porcentaje de la diferencia total entre el valor máximo y mínimo y usarlo
    como rango máximo en los buckets. Se comienza computando un step distribution y se dividen los buckets muy grandes
    para achicar los rangos. """


    def build_struct(self):
        # Find min and max
        max_value, min_value, self.total_tuples = self.db.select_rows("max( " + self.column + "), min( " + self.column + "), count( " + self.column + ")", self.table)[0]

        # Build bins dictionary as: [start: (finish, tuples)]
        # This first run is exactly as the distribution steps
        self.step_tuples = int(np.ceil(self.total_tuples/self.parameter))
        partial_bins = []
        partial_start = []
        start_value = min_value
        #TODO: generar bien las listas
        for i in range(0, self.parameter):
            where_clause = " ORDER BY %s ASC LIMIT 1 OFFSET %d" % (self.column,(i+1)*self.step_tuples)
            query = self.db.select_rows(self.column, self.table, where_clause)
            if (len(query) > 0):
                next_bin = query[0][0]
            partial_bins.append((next_bin, self.step_tuples))
            partial_start.append(start_value)
            start_value = next_bin

        max_range = np.ceil((max_value - min_value) * self.second_parameter)

        all_bins = []
        all_keys = []

        # Second run will divide ranges too large into two bins
        # Final dictionary will be like in DistributionStep {start: tuples}
        for index, start in enumerate(partial_start):
            finish, tuples = partial_bins[index][0], partial_bins[index][1]
            if finish - start > max_range:
                new_bin_start = start + max_range
                where_clause = " WHERE %s >= %d AND %s < %d" % (self.column,start,self.column,start+max_range-1)
                new_count = self.db.select_rows("count( " + self.column + ")", self.table, where_clause)[0][0]
                all_bins.append(new_count)
                all_keys.append(start)
                # Adding new values at the end to be processed. Will re-sort list later
                partial_bins.append((finish, tuples - new_count))
                partial_start.append(new_bin_start)
            else:
                all_bins.append(tuples)
                all_keys.append(start)

        #print all_keys
        #print all_bins
        self.bins = []
        self.keys = []

        i = 0
        length = len(all_keys)
        while i < length:
            min_value = min(all_keys)
            self.keys.append(min_value)
            index = all_keys.index(min_value)
            self.bins.append(all_bins[index])
            del all_keys[index]
            del all_bins[index]
            i += 1

        self.keys.append(max_value)
        self.bins.append(0)

        #print "Struct de MaxDiffVariation"
        #print "Range: ", max_range
        #print self.keys
        #print self.bins

    def estimate_equal(self, value):
        max_value = self.keys[len(self.keys) - 1]
        min_value = self.keys[0]
        is_out_of_range = value > max_value or value < min_value
        if is_out_of_range:
            return 0.0
        bin = self.bin_for(value)
        I = 0.0
        if (self.keys[bin] != value):
            return (1.0/3.0)* self.bins[bin]/self.total_tuples
        if (bin < (len(self.keys) - 1)):
            I = (1.0/2.0) * self.bins[bin]
        for i in reversed(range(0,bin)):
            if (self.keys[i] == self.keys[i+1]):
                I += self.bins[i]
            else:
                I += (1.0/2.0) * self.bins[i-1]
                break
        return I/self.total_tuples

    def estimate_greater(self, value):
        max_value = self.keys[len(self.keys) - 1]
        min_value = self.keys[0]
        is_out_of_range = value > max_value or value < min_value
        if is_out_of_range:
            if value >= max_value:
                return 0.0
            if value < min_value:
                return 1.0
        bin = self.bin_for(value)
        I = 0.0
        if (bin < (len(self.keys) - 1)):
            for i in range(bin+1, len(self.keys) - 1):
                I += self.bins[i]

        if (self.keys[bin] != value):
            I += 1.0/3.0 * self.bins[bin]
        else:
            I += 1.0/2.0 * self.bins[bin]
        return I/self.total_tuples

class ExactEstimator(Estimator):
    """Estimador Exacto"""

    def build_struct(self):
        col = self.column
        tabla = self.table
        self.total_tuples = self.db.select_rows("count(%s" % (col)+")", tabla)[0][0]

    def estimate_equal(self, value):
        col = self.column
        tabla = self.table
        str_select = "count(%s" % (col)+")"
        str_guarda = " WHERE %s = %d" % (col, value)
        frecuencia =  float(self.db.select_rows(str_select, tabla, str_guarda)[0][0])
        return frecuencia/self.total_tuples

    def estimate_greater(self, value):
        col = self.column
        tabla = self.table
        str_select = "count(%s" % (self.column)+")"
        str_guarda = " WHERE %s > %d" % (self.column, value)
        frecuencia =  float(self.db.select_rows(str_select, self.table, str_guarda)[0][0])
        return frecuencia/self.total_tuples
