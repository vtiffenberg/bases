# -*- coding: utf-8 -*-

# interfazbd.py : Implementar las consultas.
import sqlite3

class Database:
    """Clase que representa a las bases de datos."""

    # 1. Crear una BD.
    def __init__(self, path):
        self.connection = sqlite3.connect(path)
        self.cursor = self.connection.cursor()

    # 2. Crear una tabla en la BD anterior e insertar registros.
    def create_table(self, name, string_of_tuple_of_columns):
        self.cursor.execute('CREATE TABLE ' + name + string_of_tuple_of_columns)
        self.connection.commit()

    def insert_many(self, table, rows_of_tuples):
        #Ejemplo de uso:
        #purchases =[...,('2006-03-28', 'BUY', 'IBM', 1000, 45.00),...]
        #db.insert_many('stocks', purchases)
        self.valid_rows_of_tuples(rows_of_tuples)
        value_placeholders = ('?,' * len(rows_of_tuples[0]))[:-1]
        self.cursor.executemany('INSERT INTO ' + table + ' VALUES ('+ value_placeholders + ')', rows_of_tuples)
        self.connection.commit()

    # 3. Realizar distintas consultas sobre la tabla creada y mostrar los resultados.
    def select_rows(self, columns, table, conditions=''):
        self.cursor.execute("SELECT "+ columns + " FROM " + table + conditions)
        return self.cursor.fetchall()

    # 4. Realizar actualizaciones sobre los datos cargados.
    def update_table(self, table, final_values, condition):
        self.cursor.execute('UPDATE ' + table + ' SET ' + final_values +' WHERE ' + condition)
        self.connection.commit()

    # 5. Borrar algunos de los registros cargados.
    def delete_rows(self, table, conditions):
        self.cursor.execute('DELETE FROM ' + table + ' WHERE ' + conditions)
        self.connection.commit()

    # 6. Mostrar todas las tablas definidas en la BD.
    # def show_all_tables(self):

    # 7. Agregar índices sobre algunos de los campos de la tabla.
    def add_index(self, name, table, columns):
        self.cursor.execute('CREATE INDEX ' + name + ' ON ' + table + ' (' + columns + ')')
        self.connection.commit()

    # 8. Listar todas las tablas de la BD.
    def list_tables(self):
        self.cursor.execute("SELECT name FROM sqlite_master WHERE type = 'table'")
        res = []
        for row in self.cursor.fetchall():
            res.append(row[0])
        return res

    # 9. Listar todos los índices de una tabla.
    def list_indexes_for(self, table):
        self.cursor.execute("SELECT name FROM sqlite_master WHERE type = 'index' AND tbl_name = '" + table + "'")
        res = []
        for row in self.cursor.fetchall():
            res.append(row[0])
        return res

    # 10. Listar todos las columnas de una tabla.
    def list_columns_for(self, table):
        self.cursor.execute('PRAGMA table_info(' + table + ')')
        res = []
        for column in self.cursor.fetchall():
            res.append(column[1])
        return res


    def column_type(self, table, column):
        self.cursor.execute('PRAGMA table_info(' + table + ')')
        res = ""
        for row in self.cursor.fetchall():
            if row[1] == column:
                res = row[2]
        return res

#-------PRIVATE----------#
    def valid_rows_of_tuples(self,rows_of_tuples):
        #Se asegura que todas las tuplas tengan la misma
        #cantidad de elementos
        assert len(rows_of_tuples)>0, "The rows of tuples should not be empty"
        first = rows_of_tuples[0]
        len_of_first = len(first)
        for row in rows_of_tuples:
            assert len(row) == len_of_first, "The rows of tuples have different sizes"


