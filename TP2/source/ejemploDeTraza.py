# -*- coding: utf-8 -*-

from interfazbd import *

db = Database(':memory:')
db.create_table('stocks', '(date text, trans text, symbol text, qty real, price real)')


purchases = [('2006-03-28', 'BUY', 'IBM', 1000, 45.00),
             ('2006-04-05', 'BUY', 'MSFT', 1000, 72.00),
             ('2006-04-06', 'SELL', 'IBM', 500, 53.00),
            ]

db.insert_many('stocks', purchases)

print "Rows inserted in Stocks table:"
for row in db.select_rows('*', 'stocks'):
  print row

db.update_table('stocks', "date='2006-03-20'", "date='2006-03-28'")

print "\nAfter update:"
for row in db.select_rows('*', 'stocks'):
  print row

db.delete_rows('stocks', "date='2006-04-05'")

print "\nAfter delete:"
for row in db.select_rows('*', 'stocks'):
  print row

# print "\nAll tables:"
# print db.show_all_tables()

db.add_index('on_symbol', 'stocks', 'symbol')

print "\nAll indexes in table Stocks:"
print db.list_indexes_for('stocks')

print "\nAll tables in DB:"
print db.list_tables()

print "\nAll columns in table Stocks:"
print db.list_columns_for('stocks')
