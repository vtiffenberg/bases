
# -*- coding: utf-8 -*-
import os, sys, numpy
lib_path = os.path.abspath('..')
sys.path.append(lib_path)

from interfazbd import *
raiz_nombre = 'caso'
final_nombre = '.sqlite3'

def crearCasoUno():
    print "------CASO 1------"
    archivo = raiz_nombre + '1' + final_nombre
    print "Archivo: " + archivo
    print "-El valor '5' repetido 100 veces en la tabla."
    print "-Dividido en 10 partes (parameter = 10)."
    db = Database(archivo)
    db.create_table('table1', '(c1 int)')
    a_insertar = []
    for i in range(0,100):
        a_insertar.append((5,))
    db.insert_many('table1', a_insertar)


def crearCasoDos():
    print "------CASO 2------"
    archivo = raiz_nombre + '2' + final_nombre
    print "Archivo: " + archivo
    print "-100 tuplas incrementando la cantidad de valores repetidos linealmente en la tabla."
    print "-Dividido en 10 partes (parameter = 10)."
    print "-Valores del 1 al 5 "
    db = Database(archivo)
    db.create_table('table1', '(c1 int)')
    a_insertar = []
    for i in range(0,6):
        a_insertar.append((1,))
    for i in range(6,12):
        a_insertar.append((2,))
    for i in range(12,25):
        a_insertar.append((3,))
    for i in range(25,50):
        a_insertar.append((4,))
    for i in range(50,100):
        a_insertar.append((5,))
    db.insert_many('table1', a_insertar)

print "----CREANDO ARCHIVOS .db"


def crearCasoNormal():
    print "------CASO Normal------"
    archivo = raiz_nombre + 'Normal' + final_nombre
    print "Archivo: " + archivo
    print "-Diez mil tuplas con distribucion normal entre con media 0 y desviacion standar = 100"
    db = Database(archivo)
    db.create_table('table1', '(c1 int)')
    a_insertar = numpy.random.normal(0.0,100,10000)
    db.insert_many('table1', [(int(i),) for i in a_insertar])


def crearCasoUniforme():
    print "------CASO Uniforme------"
    archivo = raiz_nombre + 'Uniforme' + final_nombre
    print "Archivo: " + archivo
    print "-Diez mil tuplas con distribucion uniforme entre -1000 y 1000"
    db = Database(archivo)
    db.create_table('table1', '(c1 int)')
    a_insertar = numpy.random.random_integers(-500,500,10000)
    db.insert_many('table1', [(int(i),) for i in a_insertar])

crearCasoUno()
crearCasoDos()
crearCasoNormal()
crearCasoUniforme()
