# -*- coding: latin-1 -*-

# DATOS SOBRE ESTE SCRIPT
# 1) Está TODO hardcodeado (archivos, parámetros, valores...)
# 2) Dado un archivo, genera los gráficos para una lista de parámetros y una lista de valores
# 3) Asume que sólo hay 3 estimadores (4 si contaramos el Exacto)
# 4) Asume que no se necesitan parametrizar muchas cosas
# 5) Para cambiar funcionamiento, cambiar valores hardcodeados
# 6) Los valores hardcodeados están al final de todo

import estimators
from interfazbd import Database
import numpy
import graficadores as graf
from interfazbd import Database


class BundleDatosPorParametro(object):
    def __init__(self, mediaError, distDeMediaAlMin, distDeMediaAlMax):
        self.distDeMediaAlMin = distDeMediaAlMin
        self.mediaError = mediaError
        self.distDeMediaAlMax = distDeMediaAlMax
class BundleDeArraysIgualdadYMayor(object):
    def __init__(self, array_media_error_igualdad, array_distMediaAlMin_error_igualdad, array_distMediaAlMax_error_igualdad, array_media_error_mayor, array_distMediaAlMin_error_mayor, array_distMediaAlMax_error_mayor):
      self.array_media_error_igualdad          = array_media_error_igualdad
      self.array_distMediaAlMin_error_igualdad = array_distMediaAlMin_error_igualdad
      self.array_distMediaAlMax_error_igualdad = array_distMediaAlMax_error_igualdad
      self.array_media_error_mayor             = array_media_error_mayor
      self.array_distMediaAlMin_error_mayor    = array_distMediaAlMin_error_mayor
      self.array_distMediaAlMax_error_mayor    = array_distMediaAlMax_error_mayor

def calcularBundlePara(estimador, exacto, valores):
   #Array de todos los errores de un parámetro:
   erroresIgualdad = []
   erroresMayor = []
   for val in valores:
       #Calculo la estimación por estimador
       estimacion_igualdad = estimador.estimate_equal(val)
       estimacion_mayor = estimador.estimate_greater(val)
       #Calculo el valor real
       exacto_igualdad = exacto.estimate_equal(val)
       exacto_mayor = exacto.estimate_greater(val)
       #Agrego los errores:
       erroresIgualdad.append(abs(estimacion_igualdad-exacto_igualdad))
       erroresMayor.append(abs(estimacion_mayor-exacto_mayor))
   #Ya terminó todos los valores para este parámetro: ahora armar el bundle resultado
   #Errores por igualdad
   mediaErrorIgualdad = numpy.mean(erroresIgualdad)
   distDeMediaAlMinParaIgualdad = abs(min(erroresIgualdad) -mediaErrorIgualdad)
   distDeMediaAlMaxParaIgualdad = abs(max(erroresIgualdad) -mediaErrorIgualdad)
   bundle_igualdad = BundleDatosPorParametro(mediaErrorIgualdad, distDeMediaAlMinParaIgualdad, distDeMediaAlMaxParaIgualdad)
   #Errores por mayor
   mediaErrorMayor = numpy.mean(erroresMayor)
   distDeMediaAlMinParaMayor = abs(min(erroresMayor)-mediaErrorMayor)
   distDeMediaAlMaxParaMayor = abs(max(erroresMayor)-mediaErrorMayor)
   bundle_mayor = BundleDatosPorParametro(mediaErrorMayor, distDeMediaAlMinParaMayor, distDeMediaAlMaxParaMayor)

   return (bundle_igualdad, bundle_mayor)

def obtenerArraysPara(claseDelEstimador, archivoDB, tabla, col, exacto, parametros, valores, prefijo_arch_salida):
   #Arrays para igualdad
   array_media_error_igualdad = []
   array_distMediaAlMin_error_igualdad = []
   array_distMediaAlMax_error_igualdad = []

   #Arrays para mayor:
   array_media_error_mayor = []
   array_distMediaAlMin_error_mayor = []
   array_distMediaAlMax_error_mayor = []
   for param in parametros:
      estimador = claseDelEstimador(archivoDB, tabla, col, param)
      #Calculo para el parámetro su respectivo errorBar
      (bundle_igualdad, bundle_mayor) = calcularBundlePara(estimador, exacto, valores)
      #Agrego los resultados a igualdad
      array_media_error_igualdad.append(bundle_igualdad.mediaError)
      array_distMediaAlMin_error_igualdad.append(bundle_igualdad.distDeMediaAlMin)
      array_distMediaAlMax_error_igualdad.append(bundle_igualdad.distDeMediaAlMax)
      #Agrego los resultados a mayor
      array_media_error_mayor.append(bundle_mayor.mediaError)
      array_distMediaAlMin_error_mayor.append(bundle_mayor.distDeMediaAlMin)
      array_distMediaAlMax_error_mayor.append(bundle_mayor.distDeMediaAlMax)
   #Ya calculó todos la información por errorBar, ahora a graficarlos:
   return BundleDeArraysIgualdadYMayor(array_media_error_igualdad, array_distMediaAlMin_error_igualdad, array_distMediaAlMax_error_igualdad, array_media_error_mayor, array_distMediaAlMin_error_mayor, array_distMediaAlMax_error_mayor)

def generarTodoPara(archivoDB, tabla, col, parametros, valores):
  exacto = estimators.ExactEstimator(archivoDB, tabla, col)
  prefijoArchivo_sin_extension = archivoDB.split('.')[0]
  prefijoArchivo_sin_path= prefijoArchivo_sin_extension.split('/')[1]
  prefijoArchivo = "graficos/" + prefijoArchivo_sin_path
  print "Generando arrays para Classic:"
  classicClass = estimators.ClassicHistogram
  arrays_classic = obtenerArraysPara(classicClass, archivoDB, tabla, col, exacto, parametros, valores, prefijoArchivo + "-classic")

  print "Generando arrays para DistributionSteps:"
  dsClass = estimators.DistributionSteps
  arrays_DS = obtenerArraysPara(dsClass, archivoDB, tabla, col, exacto, parametros, valores, prefijoArchivo + "-DS")
  print "Generando arrays para MDV"
  mdvClass = estimators.MaxDiffVariation
  arrays_MDV = obtenerArraysPara(mdvClass, archivoDB, tabla, col, exacto, parametros, valores, prefijoArchivo + "-MDV")
  graficarTodoPara(len(valores), parametros, arrays_classic, arrays_DS, arrays_MDV, prefijoArchivo)

def graficarTodoPara(len_valores, parametros, arrays_classic, arrays_DS, arrays_MDV, prefijoArchivo):
  #graficarSeparados(len_valores, parametros, arrays_classic, arrays_DS, arrays_MDV, prefijoArchivo)
  graficarJuntos(len_valores, parametros, arrays_classic, arrays_DS, arrays_MDV, prefijoArchivo)

def graficarSeparados(len_valores, parametros, arrays_classic, arrays_DS, arrays_MDV, prefijoArchivo ):
  print "Generando los gráficos individuales ..."
  #Classic
  graf.errorBars(len_valores, parametros, arrays_classic.array_media_error_igualdad, arrays_classic.array_distMediaAlMin_error_igualdad, arrays_classic.array_distMediaAlMax_error_igualdad, prefijoArchivo + "-classic-igualdad.png")
  graf.errorBars(len_valores, parametros, arrays_classic.array_media_error_mayor, arrays_classic.array_distMediaAlMin_error_mayor, arrays_classic.array_distMediaAlMax_error_mayor, prefijoArchivo + "-classic-mayor.png")
  #DS
  graf.errorBars(len_valores, parametros, arrays_DS.array_media_error_igualdad, arrays_DS.array_distMediaAlMin_error_igualdad, arrays_DS.array_distMediaAlMax_error_igualdad, prefijoArchivo + "-DS-igualdad.png")
  graf.errorBars(len_valores, parametros, arrays_DS.array_media_error_mayor, arrays_DS.array_distMediaAlMin_error_mayor, arrays_DS.array_distMediaAlMax_error_mayor, prefijoArchivo + "-DS-mayor.png")
#MDV
  graf.errorBars(len_valores, parametros, arrays_MDV.array_media_error_igualdad, arrays_MDV.array_distMediaAlMin_error_igualdad, arrays_MDV.array_distMediaAlMax_error_igualdad, prefijoArchivo + "-MDV-igualdad.png")
  graf.errorBars(len_valores, parametros, arrays_MDV.array_media_error_mayor, arrays_MDV.array_distMediaAlMin_error_mayor, arrays_MDV.array_distMediaAlMax_error_mayor, prefijoArchivo + "-MDV-mayor.png")

def graficarJuntos(len_valores, parametros, arrays_classic, arrays_DS, arrays_MDV, prefijoArchivo ):
  print "Generando los gráficos juntos ..."
  graf.errorBarsJuntos(len_valores, parametros, arrays_classic, arrays_DS, arrays_MDV, prefijoArchivo + "-todos")

#DESDE ACÁ SERÍA COMO EL "main":

archivos = ['casoUniforme.sqlite3','casoNormal.sqlite3']
# archivos = ['casoUniforme.sqlite3']
parametros = [10, 20, 30, 50, 100]
# Valores Uniforme
# valores = [-500, -300, -100, -50, 0, 50, 100, 300, 500]
# Valores Normal
valores = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]

tabla ='table1'
col = 'c1'

for arch in archivos:
    path_arch = "databases/" + arch
    generarTodoPara(path_arch, tabla, col, parametros, valores)
